<?php
include"header.php";
include"admin/db.php";
?>


<!-- main body - start
			================================================== -->
			<main>


				<!-- banner_section - start
				================================================== -->
				<!-- <section  class="banner_section software_banner deco_wrap d-flex align-items-center text-white clearfix">
					<div class="container">
						<div class="row align-items-center justify-content-lg-between">

						<div class="deco_image bg_shape_1" data-background="assets/images/banner/10_software/bg_1.png"></div>
					<div class="deco_image bg_shape_2" data-background="assets/images/banner/10_software/bg_2.png"></div>
					

					
					

							

						</div>
					</div>

					
				</section> -->
				<!-- banner_section - end
				================================================== -->
			<!--
    ####################################################
    C A R O U S E L
    ####################################################
    -->
    <div id="carousel-2" class="carousel slide carousel-fade" data-ride="carousel" data-interval="6000">
        <ol class="carousel-indicators">
            <li data-target="#carousel-2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-2" data-slide-to="1"></li>
            <li data-target="#carousel-2" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          
            <div class="carousel-item active">
                <a href="new/signin.php">

                 <img src="assets/111.jfif" alt="responsive image" class="d-block img-fluid">

                    <div class="carousel-caption">
                        <div>
                            <!-- <h2>Spike Returns</h2>
                            <p>Get Started</p> -->
                            <span class="btn btn-sm btn-outline-secondary">Learn More</span>
                        </div>
                        <br>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
          
          
            <div class="carousel-item">
                <a href="traders.php">
                 <img src="assets/112.jfif" alt="responsive image" class="d-block img-fluid">

                    <div class="carousel-caption justify-content-center align-items-center">
                        <div>
                           <!--  <h2>Every project begins with a sketch</h2>
                            <p>We work as an extension of your business to explore solutions</p> -->
                            <span class="btn btn-sm btn-outline-secondary">View Traders</span>
                        </div>
                        <br>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
            <div class="carousel-item">
                <a href="contact.php">
  
                 <img src="assets/113.jpg" alt="responsive image" class="d-block img-fluid">


                    <div class="carousel-caption justify-content-center align-items-center">
                        <div>
                            
                            <span class="btn btn-sm btn-secondary">Contact</span>
                        </div>
                        <br>
                    </div>
                </a>
            </div>
            <!-- /.carousel-item -->
        </div>
        <!-- /.carousel-inner -->
        <a class="carousel-control-prev" href="#carousel-2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->


<div class="container-fluid">
    <!-- <p>Full width carousel</p> -->
</div>
<!-- /.container -->
<style type="text/css">
	/*
Removes white gap between slides
*/
.carousel {
  background:#444;
}

/*
Forces image to be 100% width and not max width of 100%
*/
.carousel-item .img-fluid {
  width:100%;
  height:auto;
}

/* 
anchors are inline so you need ot make them block to go full width
*/
.carousel-item a {
  display: block;
  width:100%;
}

</style>
		<!-- feature_section - start
				================================================== -->
				<section id="feature_section" class="feature_section sec_ptb_120 clearfix">
					<div class="container">
						<div class="section_title text-center mb-80" data-aos="fade-up" data-aos-delay="300">
							<!-- <h3 class="sub_title mb-15">About</h3> -->
							<h2 class="title_text mb-0">About Us</h2>
						</div>

						<div class="feature_item">
							<div class="row align-items-center justify-content-lg-between justify-content-md-center justify-content-sm-center">

								<div class="col-lg-4 col-md-12 order-last">
									<div class="feature_image_2 scene">
										<div class="phone_image">
											<div class="layer" data-depth="0.2">
												<div data-aos="fade-up" data-aos-delay="100">
													<img src="assets/logo.jpeg" alt="image_not_found" style="width: 100%;">
												</div>
											</div>
										</div>
										<div class="image_2">
											<div class="layer" data-depth="0.4">
												<!-- <div data-aos="fade-up" data-aos-delay="300">
													<img src="assets/images/features/img_5.png" alt="image_not_found" data-parallax='{"y" : 70}'>
												</div> -->
											</div>
										</div>
										<div class="image_3">
											<div class="layer" data-depth="0.6">
												<!-- <div data-aos="fade-up" data-aos-delay="500">
													<img src="assets/images/features/img_6.png" alt="image_not_found" data-parallax='{"y" : 90}'>
												</div> -->
											</div>
										</div>
									</div>
								</div>


								<div class="col-lg-6 col-md-7 col-sm-8">
									<div class="section_title increase_size" data-aos="fade-up" data-aos-delay="300">
										<h2 class="title_text mb-30">
											The Best Returns is with Maximus PAMM
										</h2>
										<p class="mb-0" style="text-align: justify;">
											A PAMM account is a separate  MetaTrader account where the funds of all investors are aggregated and where the money manager performs trading. Funds in PAMM accounts are managed exclusively by the plugin and should not be managed externally. Each PAMM account is linked to the money manager’s personal (or owner) account. Each money manager may have multiple PAMM accounts. The number of active PAMM accounts per each money manager is limited by money manager configurations.<br>
Join MAXIMUS PAMM for a hassle free investment and consistent returns, because during these COVID days, a second and steady income can give you peace. Why work when you can relax while we work for you?
										</p>
										<!-- <div class="row">
											<div class="col-lg-6 col-md-6 col-sm-6">
												<div class="child_feature" data-aos="fade-up" data-aos-delay="400">
													<div class="item_icon icon_purple" data-background="assets/images/shapes/shape_17.png" style="background-image: url(_assets/images/shapes/shape_17.html);">
														<i class="icon-verified-avatar"></i>
														<span class="icon_deco icon_deco_1">
															<img src="assets/images/shapes/shape_3.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_2">
															<img src="assets/images/shapes/shape_4.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_3">
															<img src="assets/images/shapes/shape_5.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_4">
															<img src="assets/images/shapes/shape_6.png" alt="shape_not_found">
														</span>
													</div>
													<h3 class="item_title">Easy Sign up</h3>
													<p class="mb-0">
													
													</p>
												</div>
											</div>

											<div class="col-lg-6 col-md-6 col-sm-6">
												<div class="child_feature" data-aos="fade-up" data-aos-delay="500">
													<div class="item_icon icon_orange" data-background="assets/images/shapes/shape_18.png" style="background-image: url(_assets/images/shapes/shape_18.html);">
														<i class="icon-setup"></i>
														<span class="icon_deco icon_deco_1">
															<img src="assets/images/shapes/shape_3.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_2">
															<img src="assets/images/shapes/shape_4.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_3">
															<img src="assets/images/shapes/shape_5.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_4">
															<img src="assets/images/shapes/shape_6.png" alt="shape_not_found">
														</span>
													</div>
													<h3 class="item_title">Easy Sign up</h3>
													<p class="mb-0">
														
													</p>
												</div>
											</div>
										</div> -->


									</div>

								</div>
								
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_red">
										<i class="icon-globe"></i>
									</div>
									<h3 class="item_title">Tier 1 Licensed Brokers</h3>
									<p class="mb-0">
										Our brokers are with pedigree and at least 10 plus years old with multiple licenses and regulations.
									</p>
									<br><br>
								</div>
							</div>

							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_royalblue">
										<i class="icon-partnership"></i>
									</div>
									<h3 class="item_title">Comprehensive Reports</h3>
									<p class="mb-0">
										Watch your money work for you an earn you substantial profits anytime anywhere and have it in reports.
									</p>
								</div>
							</div>

							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_pink">
										<i class="icon-globe"></i>
									</div>
									<h3 class="item_title">Robust Trade Systems</h3>
									<p class="mb-0">
										We use the latest trading engines and research materials for smooth sail through the unruly ocean of trades.
									</p>
									<br>
								</div>
							</div>
											
											
										</div>
						</div>
						
					</div>

				</section>
				<!-- feature_section - end
				================================================== -->

<!-- service_section - start
				================================================== -->
				<section class="service_section sec_ptb_120 bg_gray deco_wrap clearfix">
					<div class="container w-1520">

						<div class="section_title text-center mb-80" data-aos="fade-up" data-aos-delay="300">
							<h3 class="sub_title mb-15">Our Traders</h3>
							<h2 class="title_text mb-0">Traders We Provide</h2>
						</div>

						<div id="service_carousel" class="service_carousel arrow_right_left owl-carousel owl-theme" data-aos="fade-up" data-aos-delay="500">
							<div class="item">
								<div class="service_boxed_2" data-background="assets/hot.png">
									<div class="item_header mb-30 clearfix">
										<div class="item_icon bg_gradient_red">
											<i class="icon-computer"></i>
										</div>
										<h3 class="item_title">
											<a href="https://pamm.hotforex.com/en/">Hotforex</a>
										</h3>
									</div>
									<p class="mb-0">
										Highly Regulated
									</p>
									<br>
									<a href="https://pamm.hotforex.com/en/manager-details?managerid=1283379" class="btn btn-danger"><b style="color: black;">Open Trading Account</b></a>
								</div>
							</div>

							<div class="item">
								<div class="service_boxed_2" data-background="assets/l.png">
									<div class="item_header mb-30 clearfix">
										<div class="item_icon bg_gradient_green">
											<i class="icon-phone-2"></i>
										</div>
										<h3 class="item_title">
											<a href="https://www.liteforex.com/">Lite Forex</a>
										</h3>
									</div>
									<p class="mb-0">
										Local Deposites / Withdrawls
									</p>
									<br>
									<a href="https://my.liteforex.com/?openPopup=%2Fregistration%2Fpopup&_ga=2.153771184.985510552.1610905599-105401154.1604500459" class="btn btn-success"><b>Open Trading Account</b></a>
								</div>
															</div>

							<div class="item">
								<div class="service_boxed_2" data-background="assets/fx2.png">
									<div class="item_header mb-30 clearfix">
										<div class="item_icon bg_gradient_blue">
											<i class="icon-edit"></i>
										</div>
										<h3 class="item_title">
											<a href="https://www.fxprimus.com/ind/">FxPRIMUS</a>
										</h3>
									</div>
									<p class="mb-0">
										Set Day Withdrawal
									</p>
									<br>
									<a href="https://clients.fxprimus.com/en/register?regulator=vu#_ga=2.167316094.1325244028.1610905712-2143485154.1607983799" class="btn btn-primary"><b>Open Trading Account</b></a>
								</div>
							</div>

						<div class="deco_image spahe_1" data-aos="fade-left" data-aos-delay="100">
							<img src="assets/images/shapes/shape_8.png" alt="spahe_not_found">
						</div>
						<div class="deco_image spahe_2" data-aos="fade-left" data-aos-delay="300">
							<img src="assets/images/shapes/shape_9.png" alt="spahe_not_found">
						</div>
						
					</div>
				</section>
				<!-- service_section - end
				================================================== -->
			

				<!-- service_section - start
				================================================== -->
				<section id="service_section" class="service_section sec_ptb_120 pb-0 mt--30 clearfix">
					<div class="container">

						<div class="row justify-content-center">
							<div class="col-lg-9 col-md-8 col-sm-10">
								<div class="section_title text-center mb-50">
									<h2 class="title_text mb-0 c_slide_in">
										
										<span class="c_slide_in_wrap1">
											<span class="c_slide_in_wrap2">
												<span class="c_slide_in_wrap3">
													Services
												</span>
											</span>
										</span>
									</h2>
								</div>
							</div>
						</div>

						<div class="row mb-80">
							<div class="col-lg-4 col-md-4 col-sm-12" data-aos="fade-up" data-aos-delay="300">
								<div class="service_boxed_1">
									<div class="icon_wrap">
										<div class="item_icon bg_gradient_blue">
											<i class="icon-graph"></i>
										</div>
										<span class="icon_deco icon_deco_1">
											<img src="assets/images/shapes/shape_3.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_2">
											<img src="assets/images/shapes/shape_4.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_3">
											<img src="assets/images/shapes/shape_5.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_4">
											<img src="assets/images/shapes/shape_6.png" alt="shape_not_found">
										</span>
									</div>
									<h3 class="item_title">
										<a href="#!">Multiple Brokers</a>
									</h3>
									<p class="mb-50">
										Choose from multiple tier one brokers with tier 1 regulation and fair practices.
									</p>
																		<br>
									<!-- <a href="case_study_details.html" class="details_btn"><i class="icon-arrow-right-style-1"></i></a> -->
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-12" data-aos="fade-up" data-aos-delay="500">
								<div class="service_boxed_1">
									<div class="icon_wrap">
										<div class="item_icon bg_gradient_green">
											<i class="icon-marketing"></i>
										</div>
										<span class="icon_deco icon_deco_1">
											<img src="assets/images/shapes/shape_3.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_2">
											<img src="assets/images/shapes/shape_4.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_3">
											<img src="assets/images/shapes/shape_5.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_4">
											<img src="assets/images/shapes/shape_6.png" alt="shape_not_found">
										</span>
									</div>
									<h3 class="item_title">
										<a href="#!">Self Deposits/Withdrawals</a>
									</h3>
									<p class="mb-50">
									Your account, your money. We do not have any control over it. Deposit and withdraw at your leisure.
									</p>
<!-- 									<a href="case_study_details.html" class="details_btn"><i class="icon-arrow-right-style-1"></i></a> -->
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-12" data-aos="fade-up" data-aos-delay="700">
								<div class="service_boxed_1">
									<div class="icon_wrap">
										<div class="item_icon bg_gradient_orange">
											<i class="icon-paper-plane"></i>
										</div>
										<span class="icon_deco icon_deco_1">
											<img src="assets/images/shapes/shape_3.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_2">
											<img src="assets/images/shapes/shape_4.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_3">
											<img src="assets/images/shapes/shape_5.png" alt="shape_not_found">
										</span>
										<span class="icon_deco icon_deco_4">
											<img src="assets/images/shapes/shape_6.png" alt="shape_not_found">
										</span>
									</div>
									<h3 class="item_title">
										<a href="#!">Dedicated Contact</a>
									</h3>
									<p class="mb-50">
										You will have a dedicated contact person for your assistance 24/7. We are happy to help.
									</p>
									<br>
									<!-- <a href="case_study_details.html" class="details_btn"><i class="icon-arrow-right-style-1"></i></a> -->
								</div>
							</div>
						</div>

						<div class="btn_wrap text-center" data-aos="fade-up" data-aos-delay="100">
							<a href="services.php" class="btn bg_default_blue">All Features</a>
						</div>
						
					</div>
				</section>
				<!-- service_section - end
				================================================== -->

				<!-- casestudy_section - start
				================================================== -->

				<section id="blog_section" class="blog_section sec_ptb_120 clearfix">


					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-9 col-md-8 col-sm-10">
								<div class="section_title text-center mb-50">
									<h2 class="title_text mb-0 c_slide_in">
										<span class="c_slide_in_wrap1">
											<span class="c_slide_in_wrap2">
												<span class="c_slide_in_wrap3">
													Blogs
												</span>
											</span>
										</span>
									</h2>
								</div>
							</div>
						</div>
						<div class="row mt--60">
							<?php
                    $sqlcat2=mysqli_query($conn,"SELECT * FROM blog WHERE del=0 ORDER BY id DESC LIMIT 3"); 
                    while($rowcat2=mysqli_fetch_array($sqlcat2))
                    {
                        ?>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<div class="blog_grid decrease_size" data-aos="fade-up" data-aos-delay="200">
									<!-- <div class="post_date">
										<strong>14</strong>
										<span>Jan</span>
									</div> -->
									<a href="#!" class="item_image">
										<img src="../admin/blog/<?php echo $rowcat2['image'];?>" alt="image_not_found">
									</a>
									<div class="item_content">
										<h3 class="item_title">
											<a href="#!"><?php echo $rowcat2['name'];?></a>
										</h3>
										<p class="mb-30">
									<?php echo $rowcat2['des'];?>
										</p>
										<div class="row">
											<div class="col-6">
												<a href="blogfullview.php" class="details_btn">Read More <i class="fal fa-long-arrow-right"></i></a>
											</div>
											<!-- <div class="col-6">
												<a href="#!" class="comment_btn float-right"><i class="far fa-comment mr-1"></i> 03 Comments</a>
											</div> -->
										</div>
									</div>
								</div>
							</div>
							<?php
						}
						?>
							
						</div>

						</div>

						
					</div>
				</section>
				<div class="btn_wrap text-center" data-aos="fade-up" data-aos-delay="100">
							<a href="blog.php" class="btn bg_default_blue">View All Blogs</a>
						</div>
				<!-- casestudy_section - end
				================================================== -->

				<!-- casestudy_section - start
				================================================== -->
				<section id="blog_section" class="blog_section sec_ptb_120 clearfix">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-9 col-md-8 col-sm-10">
								<div class="section_title text-center mb-50">
									<h2 class="title_text mb-0 c_slide_in">
										<span class="c_slide_in_wrap1">
											<span class="c_slide_in_wrap2">
												<span class="c_slide_in_wrap3">
													News
												</span>
											</span>
										</span>
									</h2>
								</div>
							</div>
						</div>
						<div class="row mt--60">
							<?php
                    $sqlcat1=mysqli_query($conn,"SELECT * FROM news WHERE del=0 ORDER BY id DESC LIMIT 3"); 
                    while($rowcat1=mysqli_fetch_array($sqlcat1))
                    {
                        ?>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<div class="blog_grid decrease_size" data-aos="fade-up" data-aos-delay="200">
									<!-- <div class="post_date">
										<strong>14</strong>
										<span>Jan</span>
									</div> -->
									<a href="#!" class="item_image">
										<img src="../admin/news/<?php echo $rowcat1['image'];?>" alt="image_not_found">
									</a>
									<div class="item_content">
										<h3 class="item_title">
											<a href="#!"><?php echo $rowcat1['name'];?></a>
										</h3>
										<p class="mb-30">
									<?php echo $rowcat1['des'];?>
										</p>
										<div class="row">
											<div class="col-6">
												<a href="blogfullview.php" class="details_btn">Read More <i class="fal fa-long-arrow-right"></i></a>
											</div>
											<!-- <div class="col-6">
												<a href="#!" class="comment_btn float-right"><i class="far fa-comment mr-1"></i> 03 Comments</a>
											</div> -->
										</div>
									</div>
								</div>
							</div>
							<?php
						}
						?>
							
						</div>

						
					</div>
				</section>
				<div class="btn_wrap text-center" data-aos="fade-up" data-aos-delay="100">
							<a href="news.php" class="btn bg_default_blue">View All News</a>
						</div>
				<!-- casestudy_section - end
				================================================== -->

				<!-- testimonial_section - start
				================================================== -->
				<section id="testimonial_section" class="testimonial_section sec_ptb_120 clearfix">
					<div class="container">
							
						<div class="row align-items-center justify-content-lg-between justify-content-md-center justify-content-sm-center">

							<div class="col-lg-6 col-md-12">
							

								<div id="testimonial_carousel_1" class="testimonial_carousel_1 owl-carousel owl-theme" data-aos="fade-up" data-aos-delay="100">
									<?php
                    $sqlcat=mysqli_query($conn,"SELECT * FROM feedback WHERE del=0"); 
                    while($rowcat=mysqli_fetch_array($sqlcat))
                    {
                        ?>

									<div class="item text-center text-white">
										
										<div class="thumbnail_wrap">
											<div class="thumbnail_image">
												<img src="../admin/blog/<?php echo $rowcat['image'];?>" alt="image_not_found">
											</div>
										</div>

										<p>
											<?php echo $rowcat['des'];?>
											
										</p>

										<h3 class="hero_name"><?php echo $rowcat['name'];?></h3>
										<span class="hero_title"><?php echo $rowcat['role'];?></span>
									
									</div>
										<?php

						}
							?>


									

								</div>
							</div>

							<div class="col-lg-5 col-md-5 col-sm-8" data-aos="fade-up" data-aos-delay="300">
								<div class="section_title increase_size">
									<h3 class="sub_title mb-15">Testimonials</h3>
									<h2 class="title_text mb-30">
										Check what our satisfied clients said
									</h2>
									<p class="mb-0">
										Why I say old chap that is, spiffing off his nut color blimey and guvnords geeza bloke knees up bobby sloshed arse 
									</p>
								</div>
							</div>
						
						</div>
					</div>

				</section>
				<!-- testimonial_section - end
				================================================== -->

			</main>
<?php
include"footer.php";
?>