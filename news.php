<?php
include"header.php";
include"admin/db.php";
?>

<!-- breadcrumb_section - start
				================================================== -->
				<section id="breadcrumb_section" class="breadcrumb_section bg_gradient_blue deco_wrap d-flex align-items-center text-white clearfix">
					<div class="container">
						<div class="breadcrumb_content text-center" data-aos="fade-up" data-aos-delay="100">
							<h1 class="page_title">News</h1>
							
						</div>
					</div>

					<div class="deco_image spahe_1" data-aos="fade-down" data-aos-delay="300">
						<img src="assets/images/shapes/shape_1.png" alt="spahe_not_found">
					</div>
					<div class="deco_image spahe_2" data-aos="fade-up" data-aos-delay="500">
						<img src="assets/images/shapes/shape_2.png" alt="spahe_not_found">
					</div>
				</section>
				<!-- breadcrumb_section - end
				================================================== -->


				<section id="blog_section" class="blog_section sec_ptb_120 clearfix">
					<div class="container">
						<div class="row mt--60">
							<?php
                    $sqlcat2=mysqli_query($conn,"SELECT * FROM news WHERE del=0"); 
                    while($rowcat2=mysqli_fetch_array($sqlcat2))
                    {
                        ?>
							<div class="col-lg-4 col-md-6 col-sm-6">
								<div class="blog_grid decrease_size" data-aos="fade-up" data-aos-delay="200">
									<!-- <div class="post_date">
										<strong>14</strong>
										<span>Jan</span>
									</div> -->
									<a href="#!" class="item_image">
										<img src="../admin/news/<?php echo $rowcat2['image'];?>" alt="image_not_found">
									</a>
									<div class="item_content">
										<h3 class="item_title">
											<a href="#!"><?php echo $rowcat2['name'];?></a>
										</h3>
										<p class="mb-30">
									<?php echo $rowcat2['des'];?>
										</p>
										<div class="row">
											<div class="col-6">
												<a href="newsfullview.php?id=<?php echo $rowcat2['id'];?>" class="details_btn">Read More <i class="fal fa-long-arrow-right"></i></a>
											</div>
											<!-- <div class="col-6">
												<a href="#!" class="comment_btn float-right"><i class="far fa-comment mr-1"></i> 03 Comments</a>
											</div> -->
										</div>
									</div>
								</div>
							</div>
							<?php
						}
						?>
							
						</div>
							</div>
						</div>
						
					</div>
				</section>

<?php
include"footer.php";

?>