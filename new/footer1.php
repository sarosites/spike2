
                <footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © Minible.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right d-none d-sm-block">
                    Crafted with <i class="fa fa-user"></i> by <a href="https://themesbrand.com/" target="_blank" class="text-reset">KS Globals</a>
                </div>
            </div>
        </div>
    </div>
</footer>
            </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- Right Sidebar -->
<div class="right-bar">
    <div data-simplebar class="h-100">
        <div class="rightbar-title px-3 py-4">
            <a href="javascript:void(0);" class="right-bar-toggle float-right">
                <i class="mdi mdi-close noti-icon"></i>
            </a>
            <h5 class="m-0">Settings</h5>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />
        <h6 class="text-center mb-0">Choose Layouts</h6>

        <div class="p-4">
            <div class="mb-2">
                <img src="assets1/images/layouts/layout-1.jpg" class="img-fluid img-thumbnail" alt="">
            </div>
            <div class="custom-control custom-switch mb-3">
                <input type="checkbox" class="custom-control-input theme-choice" id="light-mode-switch" checked />
                <label class="custom-control-label" for="light-mode-switch">Light Mode</label>
            </div>
    
            <div class="mb-2">
                <img src="assets1/images/layouts/layout-2.jpg" class="img-fluid img-thumbnail" alt="">
            </div>
            <div class="custom-control custom-switch mb-3">
                <input type="checkbox" class="custom-control-input theme-choice" id="dark-mode-switch" data-bsStyle="assets1/css/bootstrap-dark.min.css" data-appStyle="assets1/css/app-dark.min.css" />
                <label class="custom-control-label" for="dark-mode-switch">Dark Mode</label>
            </div>
    
            <div class="mb-2">
                <img src="assets1/images/layouts/layout-3.jpg" class="img-fluid img-thumbnail" alt="">
            </div>
            <div class="custom-control custom-switch mb-5">
                <input type="checkbox" class="custom-control-input theme-choice" id="rtl-mode-switch" data-appStyle="assets1/css/app-rtl.min.css" />
                <label class="custom-control-label" for="rtl-mode-switch">RTL Mode</label>
            </div>

            
        </div>

    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
        <!-- JAVASCRIPT -->

<script src="../assets1/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets1/libs/metismenu/metisMenu.min.js"></script>
<script src="../assets1/libs/simplebar/simplebar.min.js"></script>
<script src="../assets1/libs/node-waves/waves.min.js"></script>
<script src="../assets1/libs/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="../assets1/libs/jquery.counterup/jquery.counterup.min.js"></script>
<script src="../assets1/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

        <script src="../assets1/libs/twitter-bootstrap-wizard/prettify.js"></script>

        <!-- form wizard init -->
        <script src="../assets1/js/pages/form-wizard.init.js"></script>
        <!-- apexcharts -->
        <script src="../assets1/libs/apexcharts/apexcharts.min.js"></script>


        <!-- apexcharts init -->
        <script src="assets1/js/pages/apexcharts.init.js"></script>

        <script src="assets1/js/pages/dashboard.init.js"></script>
             <!-- Required datatable js -->
        <script src="assets1/libs/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="assets1/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="assets1/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="assets1/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
        <script src="assets1/libs/jszip/jszip.min.js"></script>
        <script src="assets1/libs/pdfmake/build/pdfmake.min.js"></script>
        <script src="assets1/libs/pdfmake/build/vfs_fonts.js"></script>
        <script src="assets1/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="assets1/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="assets1/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>
        
        <!-- Responsive examples -->
        <script src="assets1/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
        <script src="assets1/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

        <!-- Datatable init js -->
        <script src="assets1/js/pages/datatables.init.js"></script>

        
        <script src="assets1/libs/parsleyjs/parsley.min.js"></script>

        <script src="assets1/js/pages/form-validation.init.js"></script>

        <script src="assets1/js/app.js"></script>

    </body>



</html>