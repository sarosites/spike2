<!-- newsletter_section - start
				================================================== -->
				<section id="newsletter_section" class="newsletter_section text-white clearfix">
					<div class="container">
						<div class="newsletter_boxed p-60 bg_gradient_lightblue deco_wrap" data-aos="fade-up" data-aos-delay="100">

							<div class="row align-items-center justify-content-lg-between justify-content-md-center justify-content-sm-center">
								<div class="col-lg-5 col-md-12">
									<div class="section_title decrease_size">
										<h2 class="title_text mb-15">Subscribe Newsletter</h2>
										<!-- <p class="mb-0">
											The free demo comes with no commitments and <span class="d-block">no credit card required.</span>
										</p> -->
									</div>
								</div>
								<?php

define('DB_SERVER1','localhost');
define('DB_USER1','root');
define('DB_PASS1' ,'');
define('DB_NAME1', 'ks');
$conn = mysqli_connect(DB_SERVER1,DB_USER1,DB_PASS1,DB_NAME1);

// Check connection
if (mysqli_connect_errno())
{
echo "Failed to connect to MySQL: " . mysqli_connect_error();
 }
if(isset($_POST['submit']))
{
	$email=$_POST['email'];
$sql=mysqli_query($conn,"INSERT INTO email (email) VALUES ('".$email."')");
echo "<script>alert('Subscribe sent successfully..')</script>"; 
 echo "<script>window.location='index.php'</script>"; 
}

                     
                 ?>
          
								<div class="col-lg-6 col-md-6 col-sm-8">
									<form action="" method="POST">
										<div class="form_item mb-0">
											<input class="text-left" type="email" name="email" placeholder="Your email">
										</div>
										<br>
										 <button class="btn btn-primary" type="submit" name="submit">Send Message</button>
									</form>
								</div>

								
							</div>

						</div>
					</div>
				</section>
				<!-- newsletter_section - end
				================================================== -->
</main>
<!-- footer_section - start
			================================================== -->
			<footer id="footer_section" class="footer_section bg_dark_blue text-white deco_wrap sec_ptb_120 pb-0 clearfix">

				<div class="widget_area">
					<div class="container position-relative">
						<div class="row justify-content-lg-between">

							<div class="col-lg-3 col-md-12">
								<div class="widget about_content pr-0">
									<div class="brand_logo mb-30">
										<a href="index.html" class="brand_link">
											<img src="assets/logo.jpeg" alt="logo_not_found">
										</a>
									</div>
									<div class="copyright_text">
										<p class="mb-0">
											Copyright © 2020 All Rights Reserved <a href="#" class="author_link">Spikereturns</a>
										</p>
									</div>
								</div>
							</div>

							<div class="col-lg-2 col-md-4 col-sm-4">
								<div class="widget useful_links ul_li_block">
									<h3 class="widget_title mb-50">Spikereturns</h3>
									<ul class="clearfix">
										<li><a href="index.php">Home</a></li>
										<li><a href="blog.php">Blog</a></li>
										<li><a href="news.php">News</a></li>
										<li><a href="contact.php">Enquiry</a></li>
										<li><a href="about.php">deposit</a></li>
									</ul>
								</div>
							</div>

							<div class="col-lg-2 col-md-4 col-sm-4">
								<div class="widget useful_links ul_li_block">
									<h3 class="widget_title mb-50">Useful Pages</h3>
									<ul class="clearfix">
										<li><a href="about.php">About Us</a></li>
										<li><a href="services.php">Our Services</a></li>
										<li><a href="traders.php.">Traders</a></li>
										<li><a href="gallery.php">Gallery</a></li>
										<li><a href="contact.php">Contact Us</a></li>
									</ul>
								</div>
							</div>

							<div class="col-lg-2 col-md-4 col-sm-4">
								<div class="social_icon_rounded ul_li ml--30">
									<h3 class="widget_title mb-50">Follow Us</h3>
									<ul class="clearfix">
										<li>
											<a href="#!">
												<i class="icon-facebook"></i>
												<i class="icon-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#!">
												<i class="icon-twitter"></i>
												<i class="icon-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#!">
												<i class="icon-vimeo"></i>
												<i class="icon-vimeo"></i>
											</a>
										</li>
										<li>
											<a href="#!">
												<i class="icon-linkedin"></i>
												<i class="icon-linkedin"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
				</div>

			</footer>
			<!-- footer_section - end
			================================================== -->


		</div>


		<!-- jquery include -->
		
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!-- slider & carousel - jquery include -->
		<script src="assets/js/owl.carousel.min.js"></script>

		<!-- animation - jquery include -->
		<script src="assets/js/aos.js"></script>
		<script src="assets/js/splitting.js"></script>

		<!-- magnific popup - jquery include -->
		<script src="assets/js/magnific-popup.min.js"></script>

		<!-- isotope filter - jquery include -->
		<script src="assets/js/isotope.pkgd.min.js"></script>
		<script src="assets/js/masonry.pkgd.min.js"></script>
		<script src="assets/js/imagesloaded.pkgd.min.js"></script>

		<!-- mouse move & scroll parallax  - jquery include -->
		<script src="assets/js/parallax.min.js"></script>
		<script src="assets/js/parallax-scroll.js"></script>

		<!-- google map - jquery include -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhrdEzlfpnsnfq4MgU1e1CCsrvVx2d59s"></script>
        <script src="assets/js/gmaps.js"></script>

		<!-- mobile menu - jquery include -->
        <script src="assets/js/mCustomScrollbar.js"></script>

		<!-- custom - jquery include -->
		<script src="assets/js/custom.js"></script>


	</body>
</html>