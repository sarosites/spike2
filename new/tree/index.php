<?php 
include "config.php";
?>

<!DOCTYPE html>
<html>
<head>
    <title>Create treeview with jsTree plugin and PHP</title>

    <link rel="stylesheet" type="text/css" href="jstree/dist/themes/default/style.min.css">
    <script src="jquery-3.4.1.min.js"></script>

    <script type="text/javascript" src="jstree/dist/jstree.min.js"></script>
</head>


<body>
    <?php 
    // echo $_GET['id'];exit;
    // $folderData1 = mysqli_query($con,"SELECT * FROM member WHERE fld_delete=0 AND id='".$_GET['id']."' OR parent_id='".$_GET['id']."'");
    $folderData1 = mysqli_query($con,"SELECT * FROM member WHERE fld_delete=0 AND id='".$_GET['id']."' ");
    $folderData= mysqli_query($con,"SELECT *,FIND_IN_SET('".$_GET['id']."', parent_chain) as pos FROM member WHERE fld_delete=0  HAVING pos>0");
// $row = mysqli_fetch_assoc($folderData);
// echo"<pre>";print_r($row);
    $folders_arr = array();
    while($row1 = mysqli_fetch_assoc($folderData1)){
        $parentid = $row1['parent_id'];
        if($parentid >= '0') $parentid = "#";

        $selected = false;$opened = false;
        if($row1['id'] == 2){
            $selected = true;$opened = true;
        }
        $folders_arr[] = array(
            "id"=>$row1['id'],
            "parent"=>$parentid,
            "text"=>$row1['fullname'],
            "state" => array(
                "selected" => $selected,
                "opened"=>$opened
            ) 
        );
    }

    while($row = mysqli_fetch_assoc($folderData)){
        $parentid = $row['parent_id'];
        if($parentid == '0') $parentid = "#";

        $selected = false;$opened = false;
        if($row['id'] == 2){
            $selected = true;$opened = true;
        }
        $folders_arr[] = array(
            "id"=>$row['id'],
            "parent"=>$parentid,
            "text"=>$row['fullname'],
            "state" => array(
                "selected" => $selected,
                "opened"=>$opened
            ) 
        );
    }
    // echo"<pre>";print_r($folders_arr);

    ?>

    <!-- Initialize jsTree -->
    <div id="folder_jstree"></div>

    <!-- Store folder list in JSON format -->
    <textarea style="display:none;" id='txt_folderjsondata'><?= json_encode($folders_arr) ?></textarea>

    <!-- Script -->
    <script type="text/javascript">
    $(document).ready(function(){
        var folder_jsondata = JSON.parse($('#txt_folderjsondata').val());

        $('#folder_jstree').jstree({ 'core' : {
            'data' : folder_jsondata,
            'multiple': false
        }, });
      
    });
    </script>
</body>
</html>