<?php
include"header.php";
?>
 <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4">Wizard with progressbar</h4>

                                        <div id="progrss-wizard" class="twitter-bs-wizard">
                                            <ul class="twitter-bs-wizard-nav nav-justified">
                                                <li class="nav-item">
                                                    <a href="#progress-seller-details" class="nav-link" data-toggle="tab">
                                                        <span class="step-number mr-2">01</span>
                                                        Seller Details
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#progress-company-document" class="nav-link" data-toggle="tab">
                                                        <span class="step-number mr-2">02</span>
                                                        Company Document
                                                    </a>
                                                </li>

                                                <li class="nav-item">
                                                    <a href="#progress-bank-detail" class="nav-link" data-toggle="tab">
                                                        <span class="step-number mr-2">03</span>
                                                        Bank Details
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#progress-confirm-detail" class="nav-link" data-toggle="tab">
                                                        <span class="step-number mr-2">04</span>
                                                        Confirm Detail
                                                    </a>
                                                </li>
                                            </ul>

                                            <div id="bar" class="progress mt-4">
                                                <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"></div>
                                            </div>
                                            <div class="tab-content twitter-bs-wizard-tab-content">
                                                <div class="tab-pane" id="progress-seller-details">
                                                    <form>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-firstname-input">First name</label>
                                                                    <input type="text" class="form-control" id="progresspill-firstname-input">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-lastname-input">Last name</label>
                                                                    <input type="text" class="form-control" id="progresspill-lastname-input">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-phoneno-input">Phone</label>
                                                                    <input type="text" class="form-control" id="progresspill-phoneno-input">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-email-input">Email</label>
                                                                    <input type="email" class="form-control" id="progresspill-email-input">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label for="progresspill-address-input">Address</label>
                                                                    <textarea id="progresspill-address-input" class="form-control" rows="2"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="tab-pane" id="progress-company-document">
                                                  <div>
                                                    <form>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-pancard-input">PAN Card</label>
                                                                    <input type="text" class="form-control" id="progresspill-pancard-input">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-vatno-input">VAT/TIN No.</label>
                                                                    <input type="text" class="form-control" id="progresspill-vatno-input">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-cstno-input">CST No.</label>
                                                                    <input type="text" class="form-control" id="progresspill-cstno-input">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-servicetax-input">Service Tax No.</label>
                                                                    <input type="text" class="form-control" id="progresspill-servicetax-input">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-companyuin-input">Company UIN</label>
                                                                    <input type="text" class="form-control" id="progresspill-companyuin-input">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <label for="progresspill-declaration-input">Declaration</label>
                                                                    <input type="text" class="form-control" id="progresspill-declaration-input">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                  </div>
                                                </div>
                                                <div class="tab-pane" id="progress-bank-detail">
                                                    <div>
                                                      <form>
                                                          <div class="row">
                                                              <div class="col-lg-6">
                                                                  <div class="form-group">
                                                                      <label for="progresspill-namecard-input">Name on Card</label>
                                                                      <input type="text" class="form-control" id="progresspill-namecard-input">
                                                                  </div>
                                                              </div>
  
                                                              <div class="col-lg-6">
                                                                  <div class="form-group">
                                                                      <label>Credit Card Type</label>
                                                                      <select class="custom-select">
                                                                            <option selected>Select Card Type</option>
                                                                            <option value="AE">American Express</option>
                                                                            <option value="VI">Visa</option>
                                                                            <option value="MC">MasterCard</option>
                                                                            <option value="DI">Discover</option>
                                                                      </select>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="row">
                                                              <div class="col-lg-6">
                                                                  <div class="form-group">
                                                                      <label for="progresspill-cardno-input">Credit Card Number</label>
                                                                      <input type="text" class="form-control" id="progresspill-cardno-input">
                                                                  </div>
                                                              </div>
  
                                                              <div class="col-lg-6">
                                                                  <div class="form-group">
                                                                      <label for="progresspill-card-verification-input">Card Verification Number</label>
                                                                      <input type="text" class="form-control" id="progresspill-card-verification-input">
                                                                  </div>
                                                              </div>
                                                          </div>
                                                          <div class="row">
                                                              <div class="col-lg-6">
                                                                  <div class="form-group">
                                                                      <label for="progresspill-expiration-input">Expiration Date</label>
                                                                      <input type="text" class="form-control" id="progresspill-expiration-input">
                                                                  </div>
                                                              </div>
  
                                                          </div>
                                                      </form>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="progress-confirm-detail">
                                                    <div class="row justify-content-center">
                                                        <div class="col-lg-6">
                                                            <div class="text-center">
                                                                <div class="mb-4">
                                                                    <i class="mdi mdi-check-circle-outline text-success display-4"></i>
                                                                </div>
                                                                <div>
                                                                    <h5>Confirm Detail</h5>
                                                                    <p class="text-muted">If several languages coalesce, the grammar of the resulting</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="pager wizard twitter-bs-wizard-pager-link">
                                                <li class="previous"><a href="javascript: void(0);">Previous</a></li>
                                                <li class="next"><a href="javascript: void(0);">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
               
<?php
include"footer.php";
?>