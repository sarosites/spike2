<?php
include"header.php";

?>

<!-- breadcrumb_section - start
				================================================== -->
				<section id="breadcrumb_section" class="breadcrumb_section bg_gradient_blue deco_wrap d-flex align-items-center text-white clearfix">
					<div class="container">
						<div class="breadcrumb_content text-center" data-aos="fade-up" data-aos-delay="100">
							<h1 class="page_title">About Us</h1>
							
						</div>
					</div>

					<div class="deco_image spahe_1" data-aos="fade-down" data-aos-delay="300">
						<img src="assets/images/shapes/shape_1.png" alt="spahe_not_found">
					</div>
					<div class="deco_image spahe_2" data-aos="fade-up" data-aos-delay="500">
						<img src="assets/images/shapes/shape_2.png" alt="spahe_not_found">
					</div>
				</section>
				<!-- breadcrumb_section - end
				================================================== -->


				<!-- service_section - start
				================================================== -->
				<section id="service_section" class="service_section sec_ptb_120 clearfix">
					<div class="container">

						<div class="section_title text-center mb-80" data-aos="fade-up" data-aos-delay="100">
							<h3 class="sub_title mb-15">Our Services</h3>
							<h2 class="title_text mb-0">Services We Provide</h2>
						</div>

						<div class="tabs_nav ul_li" data-aos="fade-up" data-aos-delay="300">
							<ul class="nav" role="tablist">
								<li>
									<a class="active" data-toggle="tab" href="#website_tab">
										<span class="icon bg_gradient_blue"><i class="icon-computer"></i></span>
										<strong>Consulting</strong>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#mobile_tab">
										<span class="icon bg_gradient_red"><i class="icon-phone-2"></i></span>
										<strong style="font-size: 19px;">Entrepreneur</strong>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#uiux_tab">
										<span class="icon bg_gradient_green"><i class="icon-edit"></i></span>
										<strong>Credibility</strong>
										
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#marketing_tab">
										<span class="icon bg_gradient_purple"><i class="icon-start-up"></i></span>
										<strong>Support</strong>
									</a>
								</li>
								<li>
									<a data-toggle="tab" href="#cloud_tab">
										<span class="icon bg_gradient_orange"><i class="icon-cloud"></i></span>
										<strong>Deposits</strong>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</section>


						
				<!-- service_section - end
				================================================== -->


				


				<!-- feature_section - start
				================================================== -->
				<section id="feature_section" class="feature_section sec_ptb_120 clearfix">
					<div class="container">

						<div class="feature_item">
							<div class="row align-items-center justify-content-lg-between justify-content-md-center justify-content-sm-center">

								<div class="col-lg-4 col-md-12 order-last">
									<div class="feature_image_2 scene">
										<div class="phone_image">
											<div class="layer" data-depth="0.2">
												<div data-aos="fade-up" data-aos-delay="100">
													<img src="assets/logo.jpeg" alt="image_not_found" style="width: 100%;">
												</div>
											</div>
										</div>
										<div class="image_2">
											<div class="layer" data-depth="0.4">
												<!-- <div data-aos="fade-up" data-aos-delay="300">
													<img src="assets/images/features/img_5.png" alt="image_not_found" data-parallax='{"y" : 70}'>
												</div> -->
											</div>
										</div>
										<div class="image_3">
											<div class="layer" data-depth="0.6">
												<!-- <div data-aos="fade-up" data-aos-delay="500">
													<img src="assets/images/features/img_6.png" alt="image_not_found" data-parallax='{"y" : 90}'>
												</div> -->
											</div>
										</div>
									</div>
								</div>

								<div class="col-lg-6 col-md-7 col-sm-8">
									<div class="section_title increase_size" data-aos="fade-up" data-aos-delay="300">
										<h2 class="title_text mb-30">
											The Best Returns is with Maximus PAMM
										</h2>
										<p class="mb-0" style="text-align: justify;">
											A PAMM account is a separate  MetaTrader account where the funds of all investors are aggregated and where the money manager performs trading. Funds in PAMM accounts are managed exclusively by the plugin and should not be managed externally. Each PAMM account is linked to the money manager’s personal (or owner) account. Each money manager may have multiple PAMM accounts. The number of active PAMM accounts per each money manager is limited by money manager configurations.<br>
Join MAXIMUS PAMM for a hassle free investment and consistent returns, because during these COVID days, a second and steady income can give you peace. Why work when you can relax while we work for you?
										</p>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-6">
												<div class="child_feature" data-aos="fade-up" data-aos-delay="400">
													<div class="item_icon icon_purple" data-background="assets/images/shapes/shape_17.png" style="background-image: url(_assets/images/shapes/shape_17.html);">
														<i class="icon-verified-avatar"></i>
														<span class="icon_deco icon_deco_1">
															<img src="assets/images/shapes/shape_3.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_2">
															<img src="assets/images/shapes/shape_4.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_3">
															<img src="assets/images/shapes/shape_5.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_4">
															<img src="assets/images/shapes/shape_6.png" alt="shape_not_found">
														</span>
													</div>
													<h3 class="item_title">Easy Sign up</h3>
													<p class="mb-0">
													
													</p>
												</div>
											</div>

											<div class="col-lg-6 col-md-6 col-sm-6">
												<div class="child_feature" data-aos="fade-up" data-aos-delay="500">
													<div class="item_icon icon_orange" data-background="assets/images/shapes/shape_18.png" style="background-image: url(_assets/images/shapes/shape_18.html);">
														<i class="icon-setup"></i>
														<span class="icon_deco icon_deco_1">
															<img src="assets/images/shapes/shape_3.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_2">
															<img src="assets/images/shapes/shape_4.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_3">
															<img src="assets/images/shapes/shape_5.png" alt="shape_not_found">
														</span>
														<span class="icon_deco icon_deco_4">
															<img src="assets/images/shapes/shape_6.png" alt="shape_not_found">
														</span>
													</div>
													<h3 class="item_title">Easy Sign up</h3>
													<p class="mb-0">
														
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</section>
				<!-- feature_section - end
				================================================== -->


			</main>
			<!-- main body - end
			================================================== -->


<?php
include"footer.php";

?>