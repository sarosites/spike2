<?php
session_start();
date_default_timezone_set('Asia/Kolkata');
?>
<!DOCTYPE html>
<html lang="en">

	
<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="spikereturns">
		<meta name="keywords" content="trading,spike,spikereturns,no1 trading,trusted,best,secure,investment,forex trading,forex,FxPRIUM">

		<title>Spikereturn</title>
		<link rel="shortcut icon" href="assets/2.jpeg">

		<!-- css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

		<!-- icons - css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/icomoon.css">
		<link rel="stylesheet" type="text/css" href="assets/css/themify-icons.css">
		<link rel="stylesheet" type="text/css" href="assets/css/fontawesome-all.css">

		<!-- slider & carousel - css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">

		<!-- animation - css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/aos.css">
		<link rel="stylesheet" type="text/css" href="assets/css/animate.css">
		<link rel="stylesheet" type="text/css" href="assets/css/splitting.css">

		<!-- magnific popup - css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">

		<!-- custom - css include -->
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<script src="assets/js/jquery-3.4.1.min.js"></script>
	</head>


	<body class="home_software">


		<div class="body_wrap">


			<!-- backtotop - start -->
			<div id="thetop"></div>
			<div id="backtotop">
				<a href="#" id="scroll">
					<i class="fal fa-arrow-up"></i>
					<i class="fal fa-arrow-up"></i>
				</a>
			</div>
			<!-- backtotop - end -->

			<!-- preloader - start -->
			<!--
			<div id="preloader">
				<div id="ctn-preloader" class="ctn-preloader">
					<div class="animation-preloader">
						<div class="spinner"></div>
						<div class="txt-loading">
							<span data-text-preloader="M" class="letters-loading">
								M
							</span>
							<span data-text-preloader="A" class="letters-loading">
								A
							</span>
							<span data-text-preloader="K" class="letters-loading">
								K
							</span>
							<span data-text-preloader="R" class="letters-loading">
								R
							</span>
							<span data-text-preloader="O" class="letters-loading">
								O
							</span>
						</div>
						<p class="text-center">Loading</p>
					</div>
					<div class="loader">
						<div class="row">
							<div class="col-3 loader-section section-left">
								<div class="bg"></div>
							</div>
							<div class="col-3 loader-section section-left">
								<div class="bg"></div>
							</div>
							<div class="col-3 loader-section section-right">
								<div class="bg"></div>
							</div>
							<div class="col-3 loader-section section-right">
								<div class="bg"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			-->
			<!-- preloader - end -->


			<!-- header_section - start
			================================================== -->
			<header id="header_section" class="header_section sticky_header d-flex align-items-center text-white border_bottom clearfix">
				<div class="container">
					<div class="row align-items-center">

						<div class="col-lg-2">
							<div class="brand_logo">
								<a href="index.php" class="brand_link">
									<img src="assets/logo.jpeg" alt="logo_not_found" style="width: 100%;">
									<img src="assets/logo.jpeg" alt="logo_not_found">
								</a>
								<button type="button" class="menu_btn">
									<i class="fal fa-bars"></i>
								</button>
							</div>
						</div>

						<div class="col-lg-8">
							<nav class="main_menu ul_li_right clearfix">
								<ul class="clearfix">
									<li class="active menu_item_has_child">
										<a href="index.php">Home</a>
										
									</li>
									<li class="active menu_item_has_child">
										<a href="about.php">About</a>
										
									</li>
									<li class="active menu_item_has_child">
										<a href="services.php">services</a>
										
									</li>
									<li class="active menu_item_has_child">
										<a href="traders.php">Tradrs</a>
										
									</li>
									<li class="active menu_item_has_child">
										<a href="blog.php">Blog</a>
										
									</li>
									<li class="active menu_item_has_child">
										<a href="news.php">News</a>
										
									</li>
									<li class="active menu_item_has_child">
										<a href="gallery.php">Gallery</a>
										
									</li>
									<li class="active menu_item_has_child">
										<a href="contact.php">Contact</a>
										
									</li>

									
								</ul>
							</nav>
						</div>

						<div class="col-lg-2">
							<a href="signin.php" class="btn btn_border border_white float-right">Sign In</a>
						</div>

						
					</div>
				</div>
			</header>

			<!-- mobile sidebar menu - start -->
			<div class="sidebar-menu-wrapper">
				<div id="mobile_menu" class="mobile_menu">

					<div class="brand_logo mb-50 clearfix">
						<a href="index.html" class="brand_link">
							<img src="assets/logo.jpeg" alt="logo_not_found">
						</a>
						<span class="close_btn"><i class="fal fa-times"></i></span>
					</div>

					<div class="mobile_menu_dropdown menu_list ul_li_block mp_balancing mb-50 clearfix">
						<ul class="clearfix">
							

							<li >
										<a href="index.php">Home</a>
										
									</li>
									<li >
										<a href="about.php">About</a>
										
									</li>
									<li >
										<a href="services.php">services</a>
										
									</li>
									<li >
										<a href="traders.php">Tradrs</a>
										
									</li>
									<li >
										<a href="blog.php">Blog</a>
										
									</li>
									<li >
										<a href="news.php">News</a>
										
									</li>
									<li >
										<a href="gallery.php">Gallery</a>
										
									</li>
									<li >
										<a href="contact.php">Contact</a>
										
									</li>
						</ul>
					</div>

					<a href="signin.php" class="btn bg_default_blue">Sign In</a>

				</div>
				<div class="overlay"></div>
			</div>
			<div class="loader"></div>