<?php
session_start();
date_default_timezone_set('Asia/Kolkata');
// print_r($_SESSION);exit;


define('DB_SERVER1','localhost');
define('DB_USER1','root');
define('DB_PASS1' ,'');
define('DB_NAME1', 'ks');
$conn1 = mysqli_connect(DB_SERVER1,DB_USER1,DB_PASS1,DB_NAME1);

// Check connection
if (mysqli_connect_errno())
{
echo "Failed to connect to MySQL: " . mysqli_connect_error();
 }

?>
<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="utf-8" />
<title> USER | Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="Spikereturns" name="description" />
<meta content="" name="author" />
<!-- App favicon -->
<link rel="shortcut icon" href="assets1/images/11-121x121.png">
        <!-- Light layout Bootstrap Css -->
                <link rel="stylesheet" href="assets1/libs/twitter-bootstrap-wizard/prettify.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                 <link href="assets1/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="assets1/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- Responsive datatable examples -->
        <link href="assets1/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />   

<link href="assets1/css/bootstrap-dark.min.css" id="bootstrap-dark-style" disabled="disabled" rel="stylesheet" type="text/css" />
<link href="assets1/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="assets1/css/icons.min.css" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="assets1/css/app-dark.min.css" id="app-dark-style" disabled="disabled" rel="stylesheet" type="text/css" />
<link href="assets1/css/app-rtl.min.css" id="app-rtl-style" disabled="disabled" rel="stylesheet" type="text/css" />
<link href="assets1/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
<script src="assets1/libs/jquery/jquery.min.js"></script>
    </head>

    <body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <i class="uil-shutter-alt spin-icon"></i>
                </div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="layout-wrapper">

            <header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="assets1/images/11-121x121.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="assets1/images/11-121x121.png" alt="" height="20">
                    </span>
                </a>

                <a href="user-dashboard.php" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="assets1/images/11-121x121.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="assets1/images/11-121x121.png" alt="" height="20">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
           
        </div>
        <a href="https://www.hotforex.com/sv/en/account-types/new-live-account.html" target="_blank" class="btn btn-danger"> Hot Forex</a>
        <a href="" class="btn btn-success"> Light Forex</a><a href="" class="btn btn-primary" >FxPRIMUS </a>

        <div class="d-flex">

            <div class="dropdown d-inline-block d-lg-none ml-2">
                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="uil-search"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                    aria-labelledby="page-header-search-dropdown">
                    
                    <form class="p-3">
                        <div class="form-group m-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            

            

            <div class="dropdown d-none d-lg-inline-block ml-1">
                
            </div>

            <div class="dropdown d-inline-block">
                
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                    aria-labelledby="page-header-notifications-dropdown">
                    <div class="p-3">
                        
                    </div>
                    <div data-simplebar style="max-height: 230px;">
                        
                        
                        
                      
                    </div>
                    
                </div>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="assets1/images/users/avatar-4.jpg"
                        alt="Header Avatar">
                    <span class="d-none d-xl-inline-block ml-1 font-weight-medium font-size-15"><?php echo $_SESSION['user']; ?></span>
                    <i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <a class="dropdown-item" href="profile.php"><i class="uil uil-user-circle font-size-18 align-middle text-muted mr-1"></i> <span class="align-middle">View Profile</span></a>
                    <a class="dropdown-item" href="logout.php"><i class="uil uil-sign-out-alt font-size-18 align-middle mr-1 text-muted"></i> <span class="align-middle">Sign out</span></a>
                </div>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                    <i class="uil-cog"></i>
                </button>
            </div>
            
        </div>
    </div>
</header>
<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <!-- LOGO -->
    <div class="navbar-brand-box">
        <a href="user-dashboard.php" class="logo logo-dark">
            <span class="logo-sm">
                <img src="assets1/images/11-121x121.png" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="assets1/images/11-121x121.png" alt="" height="20">   <b>Spike Returns</b>
            </span>
        </a>

        <a href="user-dashboard.php" class="logo logo-light">
            <span class="logo-sm">
                <img src="assets1/images/11-121x121.png" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="assets1/images/11-121x121.png" alt="" height="20">
                Spike Returns
            </span>
        </a>
    </div>

    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
        <i class="fa fa-fw fa-bars"></i>
    </button>

    <div data-simplebar class="sidebar-menu-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <!-- <li class="menu-title">Menu</li> -->

                <li>
                    <a href="user-dashboard.php">
                        <i class="fa fa-clock"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                   
                   
                </li>

                <!-- <li class="menu-title">Apps</li> -->

                <li>
                    <a href="account.php" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span>My Account</span>
                    </a>
                </li>
                 <li>
                    <a href="inversment.php" class="waves-effect">
                        <i class="fa fa-money"></i>
                        <span>Inversment</span>
                    </a>
                </li>
                 <li>
                    <a href="commision.php" class="waves-effect">
                        <i class="fa fa-inr"></i>
                        <span>Profit Update</span>
                    </a>
                </li>
                 <li>
                    <a href="myincome.php" class="waves-effect">
                        <i class="fa fa-usd"></i>
                        <span>My Income</span>
                    </a>
                </li>
                 
                <?php
$ss1=mysqli_query($conn1,"SELECT * FROM member WHERE fld_delete=0 AND id='".$_SESSION['userid']."'");
$rows1=mysqli_fetch_array($ss1);
if($rows1['invest_usd']>=15000 && $rows1['status']=='Approved' && $rows1['role']!='Member')
{

                    ?>
                 <li>
                    
                    <a href="doc.docx" download class="waves-effect">
                        <i class="fa fa-address-book"></i>
                        <span>Download Doucment</span>
                    </a>
                </li>
                <?php
            }

            ?>
                 <li>
                    
                    <a target="_blank" href="../signup.php" class="waves-effect">
                        <i class="fa fa-address-book"></i>
                        <span>Add Member</span>
                    </a>
                </li>
                 <li>
                    <a href="logout.php" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span>logout</span>
                    </a>
                </li>

                

                

                

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->