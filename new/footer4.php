<section class="extFooter cid-sfi8FbLPNz" id="extFooter24-g">

    

    

    <div class="container">
        <div class="row content justify-content-center align-items-start mbr-black">
            <div class="col-12 col-md-6 col-lg-4 pb-3">
                <div class="logo pb-3 align-center">
                    <a href="https://spikereturns.com">
                        <img src="assets/images/11-121x121.jpeg" alt="Spikereturns" style="height: 3rem;">
                    </a>
                </div>
                <div class="social-list pb-3 align-center">
                    <h3 class="follow-title mbr-fonts-style pb-2 align-center display-5">
                        Spike Returns</h3>
                    <div class="soc-item">
                        <a href="https://www.instagram.com/spikereturns/" target="_blank">
                            <span class="mbr-iconfont mbr-iconfont-social socicon-instagram socicon"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="https://www.facebook.com/spikereturns2018" target="_blank">
                            <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
                        </a>
                    </div>
                    <div class="soc-item">
                        
                            <span class="mbr-iconfont mbr-iconfont-social socicon-youtube socicon"></span>
                        
                    </div>
                    
                    
                    
                </div>
                <div class="address">
                    <h3 class="address-title mbr-fonts-style pb-2 align-center display-5">
                        Address
                    </h3>
                    <p class="address-block mbr-fonts-style pb-3 align-center mbr-text display-4">No:60/36,<br>5th Cross Street, <br>Defense Colony,<br>Nandambakkam,<br>Chennai,<br>PIN:600032.</p>
                </div>
                <div class="contacts">
                    <h3 class="contacts-title mbr-fonts-style pb-2 align-center display-5">
                        Contacts
                    </h3>
                    <p class="contacts-block mbr-fonts-style align-center mbr-text display-4">
                        Email: info@spikereturns.com<br>
                        Phone: +91 (0) 7200 007 363<br>
                        Phone: +91 (0) 8608 370 233</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 opening-hours pb-3 align-center">
                <h3 class="opening-hours-title pb-3 mbr-fonts-style align-center display-5">
                    OPENING HOURS</h3>
                <div class="col-5 days-column mbr-fonts-style mbr-bold align-right mbr-text display-7">
                    <span>Mon-Fri</span>
                    <span>Saturday-Sunday</span><br>
                </div>
                <div class="col-5 hours-column mbr-fonts-style align-left mbr-text display-7">
                    <span>10 AM-6 PM</span>
                    <span>Meeting</span><br>
                </div>
            </div>
            <div class="form-1 col-sm-12 col-md-5 col-xl-4 col-lg-3" data-form-type="formoid">
                <!---Formbuilder Form--->
                <form action="https://mobirise.eu/" method="POST" class="mbr-form form-with-styler" data-form-title="My Mobirise Form"><input type="hidden" name="email" data-form-email="true" value="MxmNIxsxB3zA5LQjR00isGnA8oUxWmVSL3RBdvmtJLyjHheantIYfnw4uTZrim3xKbc+cX/FLHqaigsQetTapgwbVjmHuOTsi8sE2POHUrOpw841K4zgF8NwoaH2yblz">
                    <div class="row">
                        <div hidden="hidden" data-form-alert="" class="alert alert-success col-12">Thanks for filling out the form!</div>
                        <div hidden="hidden" data-form-alert-danger="" class="alert alert-danger col-12"></div>
                    </div>
                    <div class="dragArea row">
                        <div class="col-md-12">
                            <h3 class="mbr-fonts-style form-title pb-3 mbr-fonts-style align-center display-5">GET IN TOUCH</h3>
                        </div>
                        <div class="col-md-12 form-group " data-for="name">
                            <input type="text" name="name" placeholder="Name" data-form-field="Name" required="required" class="form-control display-7" id="name-extFooter24-g">
                        </div>
                        <div data-for="email" class="col-md-12 form-group ">
                            <input type="text" name="email" placeholder="E-mail" data-form-field="E-mail" class="form-control display-7" required="required" id="email-extFooter24-g">
                        </div>
                        <div data-for="phone" class="col-md-12 form-group ">
                            <input type="text" name="phone" placeholder="Phone" data-form-field="Phone" class="form-control display-7" id="phone-extFooter24-g">
                        </div>
                        <div data-for="message" class="col-md-12 form-group pb-2 ">
                            <textarea name="message" placeholder="Message" data-form-field="Message" class="form-control display-7" id="message-extFooter24-g"></textarea>
                        </div>
                        <div class="col-md-12 input-group-btn align-center">
                            <button type="submit" class="btn btn-form btn-primary display-4">Send</button>
                        </div>
                    </div>
                </form><!---Formbuilder Form--->
            </div>
        </div>
    </div>
</section>





  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/Typed/typed.min.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/nav-dropdown.js"></script>
  <script src="assets/dropdown/js/navbar-dropdown.js"></script>
  <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/formoid.min.js"></script>

  <!-- <script src="assets/libs/jquery/jquery.min.js"></script> -->
<script src="assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/libs/metismenu/metisMenu.min.js"></script>
<script src="assets/libs/simplebar/simplebar.min.js"></script>
<script src="assets/libs/node-waves/waves.min.js"></script>
<script src="assets/libs/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="assets/libs/jquery.counterup/jquery.counterup.min.js"></script>
        <script src="assets/js/app.js"></script>

        <script src="assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

        <script src="assets/libs/twitter-bootstrap-wizard/prettify.js"></script>

        <!-- form wizard init -->
        <script src="assets/js/pages/form-wizard.init.js"></script>
        <!-- Sweet Alerts js -->
        <script src="assets/libs/sweetalert2/sweetalert2.min.js"></script>

        <!-- Sweet alert init js-->
        <script src="assets/js/pages/sweet-alerts.init.js"></script>


  
  
  
 <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbr-arrow-up-icon mbr-arrow-up-icon-cm cm-icon cm-icon-smallarrow-up"></i></a></div>
  
</body>
</html>