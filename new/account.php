<?php
include"header1.php";
include"../admin/db.php";
session_start();
$ret=mysqli_query($conn,"SELECT * FROM invest WHERE i_delete=0 AND member_id='".$_SESSION['userid']."' ");
?>

     <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Account Info</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="user-dashboard.php">dashboard</a></li>
                    <li class="breadcrumb-item active">My Account</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->                        
                       
        
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
        
                                        <h4 class="card-title">Account Details</h4>
                                        <p class="card-title-desc">
                                        </p>
        
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Date</th>
                                                <th>Platname</th>
                                                <th>Login id (or) mail id</th>
                                                <th>Password</th>
                                                <th>Trading Id</th>
                                                <th>Wallet Id</th>
                                                <th>Amount USD</th>

                                            </tr>
                                            </thead>
        
        
                                            <tbody>
                                          

                                            <!-- st -->
                                              <?php
                                            if(!empty($ret))
                                            {
                                            $cnt=1;
                                                while ($num=mysqli_fetch_array($ret)) {
                                                  
                                            ?>

                                            <tr>
                                                <td><?php echo $cnt; ?></td>
                                                <td><?php echo date('d-m-Y',strtotime($num['i_date'])); ?></td>
                                                <td><?php echo $num['platname']; ?></td>
                                                <td><?php echo $num['pfid']; ?></td>
                                                <td><?php echo $num['pfp']; ?></td>
                                                <td><?php echo $num['iid']; ?></td>
                                                <td><?php echo $num['wid']; ?></td>
                                                <td><?php echo $num['amt1']; ?></td>
                                            </tr>
                                                
                                              
                                  <!-- <td><?php echo $num['status']; ?></td> -->
                                            </tr>
                                            <?php
                                            $cnt=$cnt+1;
                                                }
                                            }
                                            ?>
                                            <!-- ed -->
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->
                         <div class="row">
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4"> HotForex</h4>
                                        <h5>Since 10 Plus years</h5>
                                        <a href="https://www.hotforex.com/sv/en/account-types/new-live-account.html" class="btn btn-primary"> Open Trading Account</a>
                                        
                                        
                                    </div>
                                </div><!--end card-->
                            </div>
                      
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4"> LiteForex</h4>
                                        <h5>In the market for 14 plus years</h5>
                                        <a href="https://my.liteforex.com/registration/index?returnUrl=%2Fpamm%2Ftrader%3Fserver%3D81%26login%3D76101%26language_save%3D1l" class="btn btn-success"> Open Trading Account</a>
                                        
                                    </div>
                                </div><!--end card-->
                            </div>
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                       <h4 class="card-title mb-4">FxPRIMUS</h4>
                                        <h5>Since 10  years</h5>
                                        <a href="https://www.fxprimus.com/?atoken=8GAV5VM93HSARMMX" class="btn btn-info"> Open Trading Account</a>
                                        
                                        
                                    </div>
                                </div><!--end card-->
                            </div>
                        </div> 

                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


<?php
include"footer1.php";
?>