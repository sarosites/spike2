<?php
include"header.php";
?>

<section class="extHeader cid-sedEpecayq" id="extHeader13-a">

    

    <div class="mbr-overlay" style="opacity: 0.5; background-color: rgb(202, 209, 234);">
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="mbr-white col-md-12 col-lg-6 py-lg-0 pt-4 order-2">
                <div class="typed-text pb-3 align-left display-1">
                    <span class="mbr-section-subtitle mbr-fonts-style display-1">
                          <br><br>Future is
                    </span>
                    <span>
                      <span class="animated-element mbr-bold" data-word1="Here" data-word2="Now" data-word3="Lorem" data-word4="Lorem" data-word5="Ipsum" data-speed="50" data-words="2">
                      </span>
                    </span>
                </div>
                <p class="mbr-section-text mbr-fonts-style align-left display-5">
                    With our MAXIMUS PAMM, one day they will tell your story - His -Story. We mean, Your Story-that you have made history.</p>
                <div class="pt-3 mbr-section-btn align-left"><a class="btn btn-md btn-secondary display-4" type="submit" href="https://pamm.hotforex.com/hf/en/managerdetails.html?managerID=1283379" target="_blank">EXAMPLE : PERFORMANCE OF OUR PAMM WITH HOTFOREX</a></div>
            </div>
            <div class="col-lg-6 py-lg-0 pb-2">
                <div class="mbr-figure">
                    <img src="assets/images/271128-p5vsjp-57-1080x912.png" alt="" title="">
                </div>
            </div>
        </div>
    </div>
    
</section>

<section class="progress-bars3 cid-sfie7fy6iZ" id="progress-bars3-h">
    
     

    

    <div class="container">
        <div class="row">
            <div class="col-md-6 text-elements pb-3">
                <h2 class="mbr-section-title mbr-fonts-style display-2">
                    The Best Returns is with</h2>
                <h3 class="mbr-section-subtitle mbr-fonts-style pb-4 mbr-semibold display-2">
                    Maximus PAMM</h3>
                <p class="section-content-text align-left mbr-fonts-style display-7">A PAMM account is a separate&nbsp; MetaTrader account where the funds of all investors are aggregated
and where the money manager performs trading. Funds in PAMM accounts are managed exclusively
by the plugin and should not be managed externally.
Each PAMM account is linked to the money manager’s personal (or owner) account. Each money
manager may have multiple PAMM accounts. The number of active PAMM accounts per each money
manager is limited by money manager configurations.<br>Join MAXIMUS PAMM for a hassle free investment and consistent returns, because during these COVID days, a second and steady income can give you peace. Why work when you can relax while we work for you?</p>
            </div>
            <div class="progress_elements col-md-6 mt-5 pb-3">
                <div class="progress1 pb-5">
                    <div class="progress-container">
                        <span class="progress-title mbr-fonts-style display-7">
                              Consulting
                        </span>
                        <div class="progress_value">
                            <div class="progressbar-number"></div>
                            <span>%</span>
                        </div>
                        <progress class="progress progress-primary" max="100" value="100">
                        </progress>
                    </div>
                </div>
                 
                <div class="progress2 pb-5 pt-3">
                    <div class="progress-container">
                        <span class="progress-title mbr-fonts-style display-7">
                              Entrepreneurship
                        </span>
                        <div class="progress_value">
                            <div class="progressbar-number"></div>
                            <span>%</span>
                        </div>
                        <progress class="progress progress-primary" max="100" value="100">
                        </progress>
                    </div>
                </div>
                
                <div class="progress3 pb-5 pt-3">
                    <div class="progress-container">
                        <span class="progress-title mbr-fonts-style display-7">
                              Credibility</span>
                        <div class="progress_value">
                             <div class="progressbar-number"></div>
                             <span>%</span>
                        </div>
                        <progress class="progress progress-primary" max="100" value="100">
                        </progress>
                    </div>
                </div>
                
                <div class="progress4 pb-5 pt-3">
                    <div class="progress-container">
                        <span class="progress-title mbr-fonts-style display-7">
                              Support</span>
                       <div class="progress_value">
                            <div class="progressbar-number"></div>
                            <span>%</span>
                        </div>
                        <progress class="progress progress-primary" max="100" value="100">
                        </progress>
                    </div>
                </div>
            
                <div class="progress5 pb-5 pt-3">
                    <div class="progress-container">
                        <span class="progress-title mbr-fonts-style display-7">Deposits/Withdrawals/Returns</span>
                        <div class="progress_value">
                            <div class="progressbar-number"></div>
                            <span>%</span>
                        </div>
                        <progress class="progress progress-primary" max="100" value="100">
                        </progress>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="features15 cid-rWT2C7KjnH" id="features15-4">
	<div class="container justify-content-center">
		<h3 class="mbr-fonts-style mbr-section-subtitle align-center mbr-light pt-3 display-5">Since 2011</h3>
		<div class="badge_wrap justify-content-center" class="responsive">
			<h5 class="badge mbr-bold mbr-fonts-style display-4  " class="responsive">At any point, you may not need to transfer any funds to us. You may open your own account in your name and have all the controls with you. 
				<br>\
			We do not solicit any fund transfer to us. In any case, if you experience that sort of solicitation, please inform us through our direct line or email and we will take the appropriate actions.
		</h5>
	</div>
	<h2 class="mbr-fonts-style mbr-section-title align-center display-5">Welcome to the most powerful investment returns PAMM program<br><br>WELCOME TO MAXIMUS PAMM SERVICES</h2>
	<div class="row justify-content-center">
		<div class="col-sm-12 col-lg-4">
            <div class="icon-main">
                <div class="process-icon d-flex flex-row justify-content-center">
                    <div class="wrapper">
                    	<span class="icon-container">
                    		<a href="#">
                    			<span class="icon-wrapper icon1 mbr-iconfont mobi-mbri-cash mobi-mbri"></span>
                    		</a>
                                
                        </span>
                    </div>
                    <div class="text-wrap">
                    	<h1 class="subicon-title mbr-fonts-style display-7">Multiple Brokers
                    	</h1>
                         <p class="subicon-text mbr-fonts-style display-7">
                         Choose from multiple tier one brokers with tier 1 regulation and fair practices.</p>
                    </div>

                </div>
            </div>
        </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="mbr-iconfont icon2 icon-wrapper mbrib-protect"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Self Deposits/Withdrawals</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Your account, your money. We do not have any control over it. Deposit and withdraw at your leisure.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="mbr-iconfont icon3 icon-wrapper mbrib-refresh"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Dedicated Contact</h1>
                            <p class="subicon-text mbr-fonts-style display-7">You will have a dedicated contact person for your assistance 24/7. We are happy to help.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="mbr-iconfont icon4 icon-wrapper mbrib-growing-chart"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Tier 1 Licensed Brokers</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Our brokers are with pedigree and at least 10 plus years old with multiple licenses and regulations.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="icon-wrapper icon5 mbr-iconfont mbrib-setting"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Comprehensive Reports</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Watch your money work for you an earn you substantial profits anytime anywhere and have it in reports.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="icon-wrapper icon6 mbr-iconfont mbrib-globe"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Robust Trade Systems</h1>
                            <p class="subicon-text mbr-fonts-style display-7">We use the latest trading engines and research materials for smooth sail through the unruly ocean of trades.</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<section class="features15 cid-sfi7KvEiK3" id="features15-e">

    

    
    <div class="container justify-content-center">
        
        <div class="badge_wrap align-center">
            
        </div>
        
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="icon-wrapper icon1 mbr-iconfont mobi-mbri-change-style mobi-mbri"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Market Veterans</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Our team consists of market veterans who take their profession seriously and passionately.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="mbr-iconfont icon2 icon-wrapper mobi-mbri-features mobi-mbri"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Monitor 24/7</h1>
                            <p class="subicon-text mbr-fonts-style display-7">You can monitor your account 24/7 in all kind of devices. They are easy to understand.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="mbr-iconfont icon3 icon-wrapper mobi-mbri-delivery mobi-mbri"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Controls in Your Hands</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Your account and funds are always in your control. You can stop PAMM and withdraw your capital anytime. All is automated.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="mbr-iconfont icon4 icon-wrapper mobi-mbri-drag-n-drop mobi-mbri"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Daily Updates</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Your account details are send to you as reports every day at the end of the market sessions.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="icon-wrapper icon5 mbr-iconfont mobi-mbri-paper-plane mobi-mbri"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">Secure Payments</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Our payment methods are secure and safe with strict regulatory controls as well as easy to use.</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4">
                <div class="icon-main">
                    <div class="process-icon d-flex flex-row justify-content-center">
                        <div class="wrapper">
                            <span class="icon-container">
                                <a href="#">
                                    <span class="icon-wrapper icon6 mbr-iconfont mobi-mbri-rocket mobi-mbri"></span>
                                </a>
                                
                            </span>
                        </div>
                        <div class="text-wrap">
                            <h1 class="subicon-title mbr-fonts-style display-7">24/7 Support</h1>
                            <p class="subicon-text mbr-fonts-style display-7">Our team is all ready to assist you and answer your questions any time during the week. We are here to bring the world to you.</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>
<style type="text/css">
    $hoverEasing: cubic-bezier(0.23, 1, 0.32, 1);
$returnEasing: cubic-bezier(0.445, 0.05, 0.55, 0.95);

body {
  margin: 40px 0;
  font-family: "Raleway";
  font-size: 14px;
  font-weight: 500;
  background-color: #BCAAA4;
  -webkit-font-smoothing: antialiased;
}

.title {
  font-family: "Raleway";
  font-size: 24px;
  font-weight: 700;
  color: #5D4037;
  text-align: center;
}

p {
  line-height: 1.5em;
}

h1+p, p+p {
  margin-top: 10px;
}

.container {
  padding: 40px 80px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.card-wrap {
  margin: 10px;
  transform: perspective(800px);
  transform-style: preserve-3d;
  cursor: pointer;
  // background-color: #fff;
  
  &:hover {
    .card-info {
      transform: translateY(0);
    }
    .card-info p {
      opacity: 1;
    }
    .card-info, .card-info p {
      transition: 0.6s $hoverEasing;
    }
    .card-info:after {
      transition: 5s $hoverEasing;
      opacity: 1;
      transform: translateY(0);
    }
    .card-bg {
      transition: 
        0.6s $hoverEasing,
        opacity 5s $hoverEasing;
      opacity: 0.8;
    }
    .card {
      transition:
        0.6s $hoverEasing,
        box-shadow 2s $hoverEasing;
      box-shadow:
        rgba(white, 0.2) 0 0 40px 5px,
        rgba(white, 1) 0 0 0 1px,
        rgba(black, 0.66) 0 30px 60px 0,
        inset #333 0 0 0 5px,
        inset white 0 0 0 6px;
    }
  }
}

.card {
  position: relative;
  flex: 0 0 240px;
  width: 240px;
  height: 320px;
  background-color: #333;
  overflow: hidden;
  border-radius: 10px;
  box-shadow:
    rgba(black, 0.66) 0 30px 60px 0,
    inset #333 0 0 0 5px,
    inset rgba(white, 0.5) 0 0 0 6px;
  transition: 1s $returnEasing;
}

.card-bg {
  opacity: 0.5;
  position: absolute;
  top: -20px; left: -20px;
  width: 100%;
  height: 100%;
  padding: 20px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  transition:
    1s $returnEasing,
    opacity 5s 1s $returnEasing;
  pointer-events: none;
}

.card-info {
  padding: 20px;
  position: absolute;
  bottom: 0;
  color: #fff;
  transform: translateY(40%);
  transition: 0.6s 1.6s cubic-bezier(0.215, 0.61, 0.355, 1);
  
  p {
    opacity: 0;
    text-shadow: rgba(black, 1) 0 2px 3px;
    transition: 0.6s 1.6s cubic-bezier(0.215, 0.61, 0.355, 1);
  }
  
  * {
    position: relative;
    z-index: 1;
  }
  
  &:after {
    content: '';
    position: absolute;
    top: 0; left: 0;
    z-index: 0;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(to bottom, transparent 0%, rgba(#000, 0.6) 100%);
    background-blend-mode: overlay;
    opacity: 0;
    transform: translateY(100%);
    transition: 5s 1s $returnEasing;
  }
}

.card-info h1 {
  font-family: "Playfair Display";
  font-size: 36px;
  font-weight: 700;
  text-shadow: rgba(black, 0.5) 0 10px 10px;
}
</style>
<section>
    <div class="container">
        <div class="row">
            <h1 class="title">Hover over the cards</h1>

<div id="app" class="container">
  <card data-image="https://images.unsplash.com/photo-1479660656269-197ebb83b540?dpr=2&auto=compress,format&fit=crop&w=1199&h=798&q=80&cs=tinysrgb&crop=">
    <h1 slot="header">Canyons</h1>
    <p slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
  </card>
  <card data-image="https://images.unsplash.com/photo-1479659929431-4342107adfc1?dpr=2&auto=compress,format&fit=crop&w=1199&h=799&q=80&cs=tinysrgb&crop=">
    <h1 slot="header">Beaches</h1>
    <p slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
  </card>
  <card data-image="https://images.unsplash.com/photo-1479644025832-60dabb8be2a1?dpr=2&auto=compress,format&fit=crop&w=1199&h=799&q=80&cs=tinysrgb&crop=">
    <h1 slot="header">Trees</h1>
    <p slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
  </card>
  <card data-image="https://images.unsplash.com/photo-1479621051492-5a6f9bd9e51a?dpr=2&auto=compress,format&fit=crop&w=1199&h=811&q=80&cs=tinysrgb&crop=">
    <h1 slot="header">Lakes</h1>
    <p slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
  </card>
</div>
            
        </div>
        
    </div>
</section>
<script type="text/javascript">
    
    Vue.config.devtools = true;

Vue.component('card', {
  template: `
    <div class="card-wrap"
      @mousemove="handleMouseMove"
      @mouseenter="handleMouseEnter"
      @mouseleave="handleMouseLeave"
      ref="card">
      <div class="card"
        :style="cardStyle">
        <div class="card-bg" :style="[cardBgTransform, cardBgImage]"></div>
        <div class="card-info">
          <slot name="header"></slot>
          <slot name="content"></slot>
        </div>
      </div>
    </div>`,
  mounted() {
    this.width = this.$refs.card.offsetWidth;
    this.height = this.$refs.card.offsetHeight;
  },
  props: ['dataImage'],
  data: () => ({
    width: 0,
    height: 0,
    mouseX: 0,
    mouseY: 0,
    mouseLeaveDelay: null
  }),
  computed: {
    mousePX() {
      return this.mouseX / this.width;
    },
    mousePY() {
      return this.mouseY / this.height;
    },
    cardStyle() {
      const rX = this.mousePX * 30;
      const rY = this.mousePY * -30;
      return {
        transform: `rotateY(${rX}deg) rotateX(${rY}deg)`
      };
    },
    cardBgTransform() {
      const tX = this.mousePX * -40;
      const tY = this.mousePY * -40;
      return {
        transform: `translateX(${tX}px) translateY(${tY}px)`
      }
    },
    cardBgImage() {
      return {
        backgroundImage: `url(${this.dataImage})`
      }
    }
  },
  methods: {
    handleMouseMove(e) {
      this.mouseX = e.pageX - this.$refs.card.offsetLeft - this.width/2;
      this.mouseY = e.pageY - this.$refs.card.offsetTop - this.height/2;
    },
    handleMouseEnter() {
      clearTimeout(this.mouseLeaveDelay);
    },
    handleMouseLeave() {
      this.mouseLeaveDelay = setTimeout(()=>{
        this.mouseX = 0;
        this.mouseY = 0;
      }, 1000);
    }
  }
});

const app = new Vue({
  el: '#app'
});
</script>
<section class="extPricingTables cid-sfi7FiymGd" id="extPricingTables1-d">
    <div class="container">
        <div class="media-container-row">
            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pb-4" style="background-image: url('img/hot.png');">
                      <br>
                    <br>
                    <br>
                    <br><br><br><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="plan-list pb-4 align-center">
                         <div class="mbr-section-btn align-center pb-4"><a href="https://pamm.hotforex.com/hf/en/managerdetails.html?managerID=1283379" class="btn btn-primary btn-bgr display-4" target="_blank">VIEW PERFORMANCE &amp; INVEST</a></div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://www.hotforex.com/sv/en/account-types/new-live-account.html" class="btn btn-primary btn-bgr display-4" >Open Trading Account</a></div>
  
                    </div>


                </div>
            </div>   
        </div>
    </div>
    
</section>

<section class="extPricingTables cid-sfi7FiymGd" id="extPricingTables1-d" style="background-color: red;">

    

    
    <div class="container">
        <div class="media-container-row">

            <div class="plan col-12 mx-3 my-2 justify-content-center col-lg-4">
                <div class="plan-header text-center pb-4" style="background-image: url('img/hot.png');">
                   
                   
                    <br>
                    <br>
                    <br>
                    <br><br><br><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="plan-list pb-4 align-center">
                         <div class="mbr-section-btn align-center pb-4"><a href="https://pamm.hotforex.com/hf/en/managerdetails.html?managerID=1283379" class="btn btn-primary btn-bgr display-4" target="_blank">VIEW PERFORMANCE &amp; INVEST</a></div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://www.hotforex.com/sv/en/account-types/new-live-account.html" class="btn btn-primary btn-bgr display-4" >Open Trading Account</a></div>
  
                    </div>

                </div>
            </div>
            <div class="plan col-12 mx-3 my-2 justify-content-center col-lg-4">
                <div class="plan-header text-center pb-4" style="background-image: url('img/hot.png');">
                   
                   
                    <br>
                    <br>
                    <br>
                    <br><br><br><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="plan-list pb-4 align-center">
                         <div class="mbr-section-btn align-center pb-4"><a href="https://pamm.hotforex.com/hf/en/managerdetails.html?managerID=1283379" class="btn btn-primary btn-bgr display-4" target="_blank">VIEW PERFORMANCE &amp; INVEST</a></div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://www.hotforex.com/sv/en/account-types/new-live-account.html" class="btn btn-primary btn-bgr display-4" >Open Trading Account</a></div>
  
                    </div>

                </div>
            </div>
            <div class="plan col-12 mx-3 my-2 justify-content-center col-lg-4">
                <div class="plan-header text-center pb-4" style="background-image: url('img/hot.png');">
                   
                   
                    <br>
                    <br>
                    <br>
                    <br><br><br><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="plan-list pb-4 align-center">
                         <div class="mbr-section-btn align-center pb-4"><a href="https://pamm.hotforex.com/hf/en/managerdetails.html?managerID=1283379" class="btn btn-primary btn-bgr display-4" target="_blank">VIEW PERFORMANCE &amp; INVEST</a></div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://www.hotforex.com/sv/en/account-types/new-live-account.html" class="btn btn-primary btn-bgr display-4" >Open Trading Account</a></div>
  
                    </div>

                </div>
            </div>
            

            <div class="plan col-12 mx-3 my-2 justify-content-center col-lg-4">
                
                <div class="plan-body pt-4" style="background-image: url('img/4.jpg');">
                    <div class="plan-list pb-4 align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Highly Regulated
</span></li><li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Local Deposits/Withdrawals</span></li><li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Set Day Withdrawal</span></li>
                        </ul>
                    </div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://my.liteforex.com/?uid=502699629&cid=106524" class="btn btn-primary btn-bgr display-4" target="_blank">VIEW PERFORMANCE &amp; INVEST</a></div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://my.liteforex.com/registration/index?returnUrl=%2Fpamm%2Ftrader%3Fserver%3D81%26login%3D76101%26language_save%3D1" class="btn btn-primary btn-bgr display-4" >Open Trading Account</a></div>
                </div>
            </div>

            <div class="plan col-12 mx-3 my-2 justify-content-center col-lg-4">
                <div class="plan-header text-center pb-4" style="background-image: url('img/5.jpg');">
                    <h3 class="plan-title mbr-white mbr-fonts-style pb-3 pt-3 mb-5 px-3 display-7">
                        Broker 3</h3>
                    <div class="plan-price px-3">
                        <span class="price-value mbr-fonts-style display-5">&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-2">
                            </span>
                        <small class="price-term mbr-fonts-style display-7">
                            Since 10 years</small>
                    </div>
                </div>
                <div class="plan-body pt-4">
                    <div class="plan-list pb-4 align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Highly Regulated
</span></li><li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Local Deposits/Withdrawals</span></li><li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Set Day Withdrawal</span></li>
                        </ul>
                    </div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://ironfx.com" class="btn btn-primary btn-bgr display-4" target="_blank">VIEW PERFORMANCE &amp; INVEST</a></div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://www.ironfx.com/en/register" class="btn btn-primary btn-bgr display-4" >Open Trading Account</a></div>
                </div>
            </div>

            

        </div>
    
    <div class="plan col-12 mx-3 my-2 justify-content-center col-lg-4">
                <div class="plan-header text-center pb-4" style="background-image: url('img/11.jfif');">
                    <h3 class="plan-title mbr-white mbr-fonts-style pb-3 pt-3 mb-5 px-3 display-7">
                        Broker 4</h3>
                    <div class="plan-price px-3">
                        <span class="price-value mbr-fonts-style display-5">&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-2">
                            </span>
                        <small class="price-term mbr-fonts-style display-7">
                            Since 10 years</small>
                    </div>
                </div>
                <div class="plan-body pt-4" >
                    <div class="plan-list pb-4 align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Highly Regulated
</span></li><li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Local Deposits/Withdrawals</span></li><li class="list-group-item"><span style="background-color: transparent; font-size: 1rem;">Set Day Withdrawal</span></li>
                        </ul>
                    </div>
                     <div class="mbr-section-btn align-center pb-4"><a href="https://pamm.fxprimus.com/en/Pamm/MAXIMUS" class="btn btn-primary btn-bgr display-4" target="_blank">VIEW PERFORMANCE &amp; INVEST</a></div>
                    <div class="mbr-section-btn align-center pb-4"><a href="https://clients.fxprimus.com/en/register?ref=41269525" class="btn btn-primary btn-bgr display-4" >Open Trading Account</a></div>
                </div>
            </div>

            

        </div>
    </div>
</section>
<section class="cid-sfmdb8ZjsY" id="video1-l">

    
    
    <figure class="mbr-figure align-center">
        <div class="video-block" style="width: 100%;">
            <div><iframe class="mbr-embedded-video" src="https://www.youtube.com/embed/3YyolSORXs4?rel=0&amp;amp;showinfo=0&amp;autoplay=1&amp;loop=1&amp;playlist=3YyolSORXs4" width="1280" height="720" frameborder="0" allowfullscreen></iframe></div>
        </div>
    </figure>
</section>




<?php
include"footer.php";
?>