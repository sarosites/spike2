<?php
include"header.php";

?>
	<!-- breadcrumb_section - start
				================================================== -->
				<section id="breadcrumb_section" class="breadcrumb_section bg_gradient_blue deco_wrap d-flex align-items-center text-white clearfix">
					<div class="container">
						<div class="breadcrumb_content text-center" data-aos="fade-up" data-aos-delay="100">
							<h1 class="page_title">Our Traders</h1>
							<!-- <p class="mb-0">
								Faff about only a quid blower I don't want no agro bleeding chim pot burke tosser cras nice one boot fanny.
							</p> -->
						</div>
					</div>

					<div class="deco_image spahe_1" data-aos="fade-down" data-aos-delay="300">
						<img src="assets/images/shapes/shape_1.png" alt="spahe_not_found">
					</div>
					<div class="deco_image spahe_2" data-aos="fade-up" data-aos-delay="500">
						<img src="assets/images/shapes/shape_2.png" alt="spahe_not_found">
					</div>
				</section>
				<!-- breadcrumb_section - end
				================================================== -->

				<!-- service_section - start
				================================================== -->
				<section class="service_section sec_ptb_120 bg_gray deco_wrap clearfix">
					<div class="container w-1520">

						<div class="section_title text-center mb-80" data-aos="fade-up" data-aos-delay="300">
							<h3 class="sub_title mb-15">Our Services</h3>
							<h2 class="title_text mb-0">Services We Provide</h2>
						</div>

						<div id="service_carousel" class="service_carousel arrow_right_left owl-carousel owl-theme" data-aos="fade-up" data-aos-delay="500">
							<div class="item">
								<div class="service_boxed_2" data-background="assets/hot.png">
									<div class="item_header mb-30 clearfix">
										<div class="item_icon bg_gradient_red">
											<i class="icon-computer"></i>
										</div>
										<h3 class="item_title">
											<a href="https://pamm.hotforex.com/en/">Hotforex</a>
										</h3>
									</div>
									<p class="mb-0">
										Highly Regulated
									</p>
									<br>
									<a href="https://pamm.hotforex.com/en/manager-details?managerid=1283379" class="btn btn-danger"><b style="color: black;">Open Trading Account</b></a>
								</div>
							</div>

							<div class="item">
								<div class="service_boxed_2" data-background="assets/l.png">
									<div class="item_header mb-30 clearfix">
										<div class="item_icon bg_gradient_green">
											<i class="icon-phone-2"></i>
										</div>
										<h3 class="item_title">
											<a href="https://www.liteforex.com/">Lite Forex</a>
										</h3>
									</div>
									<p class="mb-0">
										Local Deposites / Withdrawls
									</p>
									<br>
									<a href="https://my.liteforex.com/?openPopup=%2Fregistration%2Fpopup&_ga=2.153771184.985510552.1610905599-105401154.1604500459" class="btn btn-success"><b>Open Trading Account</b></a>
								</div>
															</div>

							<div class="item">
								<div class="service_boxed_2" data-background="assets/fx2.png">
									<div class="item_header mb-30 clearfix">
										<div class="item_icon bg_gradient_blue">
											<i class="icon-edit"></i>
										</div>
										<h3 class="item_title">
											<a href="https://www.fxprimus.com/ind/">FxPRIMUS</a>
										</h3>
									</div>
									<p class="mb-0">
										set Day Withdrawal
									</p>
									<br>
									<a href="https://clients.fxprimus.com/en/register?regulator=vu#_ga=2.167316094.1325244028.1610905712-2143485154.1607983799" class="btn btn-primary"><b>Open Trading Account</b></a>
								</div>
							</div>

						<div class="deco_image spahe_1" data-aos="fade-left" data-aos-delay="100">
							<img src="assets/images/shapes/shape_8.png" alt="spahe_not_found">
						</div>
						<div class="deco_image spahe_2" data-aos="fade-left" data-aos-delay="300">
							<img src="assets/images/shapes/shape_9.png" alt="spahe_not_found">
						</div>
						
					</div>
				</section>
				<!-- service_section - end
				================================================== -->


<?php
include"footer.php";

?>