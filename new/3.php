
<form method="post" id="cour_form" enctype="multipart/form-data" action="<?php echo site_url('courses/add_edit');?>">
<div class="row">
	<div class="col-12 col-lg-12">
		<div class="card radius-15 border-lg-top-info">
			<div class="card-body p-5">
			

									
				<div class="form-body">
										
					<div class="form-group">
						<input type="hidden" id="id" name="id" value="<?php echo isset($cour_data['id'])?$tut_data['id']:""; ?>" />
						<label>Category</label>
						
						<select id="catname" name="catname" class="form-control radius-30">
							<option value="">--Select--</option>
							<?php  
							if(!empty($cat_list))
							{
								foreach ($cat_list as $key => $value) {
							?>
							<option value="<?php echo $value['id'] ?>"><?php echo $value['categoryname']; ?></option>
							<?php
								}
							}
							?>
							
						</select>
						<span class="catname_err text-danger"></span>
					</div>
					<div class="form-group">
						<label>Course Name </label>
						<input type="text" id="coursename" name="coursename" class="form-control radius-30" value="<?php echo isset($cour_data['coursename'])?$cour_data['coursename']:""; ?>" />
						<span class="coursename_err text-danger"></span>
					</div>
					<div class="form-group">
						<label>Question Paper</label>
						<input type="file" id="file" name="file[]" multiple="true" class="form-control radius-30" />
						<!-- <span class="exep_err text-danger"></span> -->
					</div>
					<div class="form-group">
						<label>Syllabus</label>
						<input type="file" id="file1" name="file1[]" multiple="true" class="form-control radius-30" />
						<!-- <span class="dob_err text-danger"></span> -->
					</div>
					<div class="form-group">
						<label>Online Class</label>
						<input type="text" id="onclass" name="onclass" class="form-control radius-30" value="<?php echo isset($cour_data['email'])?$cour_data['onlineclass']:""; ?>" />
						<span class="onclass_err text-danger"></span>
					</div>
					<div class="form-group">
						<label>Youtube URL</label>
						<input type="text" id="youurl" name="youurl" class="form-control radius-30" value="<?php echo isset($cour_data['youtubeurl'])?$cour_data['youtubeurl']:""; ?>" />
						<span class="youurl_err text-danger"></span>
					</div>
					<div class="form-group">
						<label>Study Material</label>
						<input type="file" id="file2" name="file2[]" multiple="true" class="form-control radius-30" />
						<!-- <span class="dob_err text-danger"></span> -->
					</div>
										
										
										
					<!-- <button type="submit" class="btn btn-primary px-5 radius-30">Register</button> -->
				</div>
			
			</div>
		</div>
	</div>
</div>
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-primary" >Save</button>
</form>  

<!-- <script type="text/javascript">
	
	$(document).ready(function(){
		$('#cour_form').submit(function(e){
			e.preventDefault();
			if($('#catname').val()=='')
			{
				$('.catname_err').text('Required');
			}
			else
			{
				$('.catname_err').text('');
			}
			if($('#coursename').val()=='')
			{
				$('.coursename_err').text('Required');
			}
			else
			{
				$('.coursename_err').text('');
			}
			if($('#onclass').val()=='')
			{
				$('.onclass_err').text('Required');
			}
			else
			{
				$('.onclass_err').text('');
			}
			if($('#youurl').val()=='')
			{
				$('.youurl_err').text('Required');
			}
			else
			{
				$('.youurl_err').text('');
			}
			if($('#catname'). val()!='' && $('#coursename'). val()!='' && $('#onclass'). val()!='' && $('#youurl'). val()!='')
			{
				 $.ajax({
			        url : '<?php echo site_url('courses/add_edit');?>',
			        type:"post",
			        data:$('#cour_form').serialize(),
			        dataType: "json",
			        success: function( data ) {
			        	// console.log(data);
			            
			            	
			                Swal.fire({
			                  position: 'top',
			                  type: 'success',
			                  title: data.title,
			                  showConfirmButton: false,
			                  timer: 1500
			              });
			               
			                location.reload();

			                
			            
			        }
    });
			}
		})
	})

</script> -->