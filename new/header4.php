<?php
session_start();
date_default_timezone_set('Asia/Kolkata');
?>
<!DOCTYPE html>
<html>
<head>
  
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/2.jpeg" type="image/x-icon">
  <meta name="description" content="">
  
  
  <title>Home</title>
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons2/mobirise2.css">
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons-bold/mobirise-icons-bold.css">
  <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="stylesheet" href="assets/recaptcha.css">
  <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css"><link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
   <!-- Sweet Alert-->
        <link href="assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
  
  
  <!-- <script src="assets/web/assets/jquery/jquery.min.js"></script> -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
  
  

</head>
<body>
  
  <section class="menu cid-rWSWT8Bh3y" once="menu" id="menu3-1">

    

    <nav class="navbar navbar-dropdown beta-menu align-items-center navbar-fixed-top navbar-toggleable-sm">
        <div class="menu-content-top order-1 order-lg-0" id="topLine">
            <div class="container">
                <div class="row ">
                    <div class="col-12 col-lg-3">
                        <div class="content-left-side">
                            <p class="mbr-fonts-style content-text mbr-black display-4">The right kind of investment &amp; return program for our times.</p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9">
                        <div class="content-right-side" data-app-modern-menu="true"><a class="content-link link mbr-black display-4" href="#">
                            <span class="mbri-mobile2 mbr-iconfont mbr-iconfont-btn "></span>
                                + (91) 7200-007-363</a>
                            <a class="content-link link mbr-black display-4" href="#">
                            <span class="mbri-clock mbr-iconfont mbr-iconfont-btn "></span>
                                Mon - Fri: 9:00AM - 5:00PM
                            </a>
                            <a class="content-link link mbr-black display-4" href="#">
                            <span class="mbri-letter mbr-iconfont mbr-iconfont-btn "></span>
                                info@spikereturns.com
                            </a>
                          <a class="content-link link mbr-black display-4" href="login.php">
                            <span class="fa fa-user" ></span>
                                Login
                            </a>
                          <a class="content-link link mbr-black display-4" href="form.php">
                            <span class="fa fa-user" >/</span>
                                Signup
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-bottom order-0 order-lg-1">

            <div class="menu-logo">
                <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="index.php">
                        <img src="assets/images/11-121x121.jpeg" alt="Spikereturns" title="" class="responsive">
                    </a>
                </span>
                    <span class="navbar-caption-wrap"><a href="#top" class="brand-link mbr-black text-primary display-5">
                        </a></span>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent, #topLine" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav nav-dropdown js-float-line" data-app-modern-menu="true"><li class="nav-item">
                        <a class="nav-link link mbr-black text-primary display-4" href="index.php#features15-4"><span class="mbri-menu mbr-iconfont mbr-iconfont-btn"></span>
                            
                            Features</a>
                    </li><li class="nav-item"><a class="nav-link link mbr-black text-primary display-4" href="index.php#extPricingTables1-d"><span class="mbri-delivery mbr-iconfont mbr-iconfont-btn"></span>
                            
                            Brokers</a></li><li class="nav-item"><a class="nav-link link mbr-black text-primary display-4" href="#top"><span class="mbri-success mbr-iconfont mbr-iconfont-btn"></span>
                            
                            Blog</a></li></ul>
                <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="index.php#extPricingTables1-d">
                    <span class="mbri-help mbr-iconfont mbr-iconfont-btn "></span>
                        PAMM SYSTEMS</a> <a class="btn btn-sm btn-success display-4" href="index.php#extFooter24-g">
                    <span class="mbri-help mbr-iconfont mbr-iconfont-btn "></span>
                        FREE CONSULTATION
                    </a>
                  </div>

            </div>
        </div>
    </nav>
</section>
