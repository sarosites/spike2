<?php
include"header.php";
include"../admin/db.php";
?>
<!-- breadcrumb_section - start
				================================================== -->
				<section id="breadcrumb_section" class="breadcrumb_section bg_gradient_blue deco_wrap d-flex align-items-center text-white clearfix">
					<div class="container">
						<div class="breadcrumb_content text-center" data-aos="fade-up" data-aos-delay="100">
							<h1 class="page_title">Our Gallery</h1>
							
						</div>
					</div>

					<div class="deco_image spahe_1" data-aos="fade-down" data-aos-delay="300">
						<img src="assets/images/shapes/shape_1.png" alt="spahe_not_found">
					</div>
					<div class="deco_image spahe_2" data-aos="fade-up" data-aos-delay="500">
						<img src="assets/images/shapes/shape_2.png" alt="spahe_not_found">
					</div>
				</section>
				<!-- breadcrumb_section - end
				================================================== -->


				<!-- gallery_section - start
				================================================== -->
				<section id="gallery_section" class="gallery_section sec_ptb_120 clearfix">
					<div class="container">

						<!-- <div class="button-group filter-btns-group ul_li_center mb-50" data-aos="fade-up" data-aos-delay="200">
							<ul class="clearfix">
								<li><button class="active" data-filter="*">All Projects</button></li>
								<li><button data-filter=".design">Design</button></li>
								<li><button data-filter=".marketing">Marketing</button></li>
								<li><button data-filter=".development">Development</button></li>
								<li><button data-filter=".branding">Branding</button></li>
							</ul>
						</div> -->

						<div class="zoom-gallery element_grid masonry_portfolio column_4">
							<?php
                    $sqlcat2=mysqli_query($conn,"SELECT * FROM gallery "); 
                    while($rowcat2=mysqli_fetch_array($sqlcat2))
                    {
                        ?>
                        <?php

                 $img=explode('|', $rowcat2['images']);
                for($i=0;$i<count($img);$i++)
                {?>
                 <!--  <div class="col-sm-4">
<img src="gallery/<?php echo isset($img[$i])?$img[$i]:""; ?>" alt="img" width="150">
</div> -->

<div class="element-item marketing design " data-category="marketing">
								<div class="casestudy_fullimage" data-aos="fade-up" data-aos-delay="500">
									<a href="../admin/gallery/<?php echo isset($img[$i])?$img[$i]:""; ?>" class="item_image popup_image">
										<img src="../admin/gallery/<?php echo isset($img[$i])?$img[$i]:""; ?>" alt="image_not_found">
									</a>
									<div class="item_content">
										<h3 class="item_title mb-0"><a class="popup_image" href="../admin/gallery/<?php echo isset($img[$i])?$img[$i]:""; ?>"><?php echo $rowcat2['title']; ?></a></h3>
										<!-- <div class="item_category">
											<a href="#!">Design</a>
										</div> -->
									</div>
								</div>
							</div>
                  <?php
}
                  ?>

							
							<?php
						}
							?>
							
							</div>
						</div>
					</section>
<?php
include"footer.php";

?>