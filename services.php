<?php
include"header.php";

?>
<!-- breadcrumb_section - start
				================================================== -->
				<section id="breadcrumb_section" class="breadcrumb_section bg_gradient_blue deco_wrap d-flex align-items-center text-white clearfix">
					<div class="container">
						<div class="breadcrumb_content text-center" data-aos="fade-up" data-aos-delay="100">
							<h1 class="page_title">Our Services</h1>
							
						</div>
					</div>

					<div class="deco_image spahe_1" data-aos="fade-down" data-aos-delay="300">
						<img src="assets/images/shapes/shape_1.png" alt="spahe_not_found">
					</div>
					<div class="deco_image spahe_2" data-aos="fade-up" data-aos-delay="500">
						<img src="assets/images/shapes/shape_2.png" alt="spahe_not_found">
					</div>
				</section>
				<!-- breadcrumb_section - end
				================================================== -->


<!-- feature_section - start
				================================================== -->
				<section >
					<div class="container">
						<div class="section_title text-white mb-50 c_slide_in">
							<h2 class="title_text mb-0">
								<span class="c_slide_in_wrap1">
									<span class="c_slide_in_wrap2">
										<span class="c_slide_in_wrap3">
											Our Best Features
										</span>
									</span>
								</span>
							</h2>
						</div>

						<div class="row">
							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_lightblue">
										<i class="icon-shield"></i>
									</div>
									<h3 class="item_title">Multiple Brokers</h3>
									<p class="mb-0">
										Choose from multiple tier one brokers with tier 1 regulation and fair practices.
									</p>
									<br><br>
								</div>
							</div>

							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_green">
										<i class="icon-ring-2"></i>
									</div>
									<h3 class="item_title">Self Deposits/Withdrawals</h3>
									<p class="mb-0">
										Your account, your money. We do not have any control over it. Deposit and withdraw at your leisure.
									</p>
								</div>
							</div>

							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_yellow">
										<i class="icon-test"></i>
									</div>
									<h3 class="item_title">Dedicated Contact</h3>
									<p class="mb-0">
										You will have a dedicated contact person for your assistance 24/7. We are happy to help.
									</p>
									<br><br>
								</div>
							</div>

							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_red">
										<i class="icon-globe"></i>
									</div>
									<h3 class="item_title">Tier 1 Licensed Brokers</h3>
									<p class="mb-0">
										Our brokers are with pedigree and at least 10 plus years old with multiple licenses and regulations.
									</p>
									<br><br>
								</div>
							</div>

							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_royalblue">
										<i class="icon-partnership"></i>
									</div>
									<h3 class="item_title">Comprehensive Reports</h3>
									<p class="mb-0">
										Watch your money work for you an earn you substantial profits anytime anywhere and have it in reports.
									</p>
								</div>
							</div>

							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_pink">
										<i class="icon-globe"></i>
									</div>
									<h3 class="item_title">Robust Trade Systems</h3>
									<p class="mb-0">
										We use the latest trading engines and research materials for smooth sail through the unruly ocean of trades.
									</p>
									<br>
								</div>
							</div>
							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_pink">
										<i class="icon-ring-2"></i>
									</div>
									<h3 class="item_title">Market Veterans</h3>
									<p class="mb-0">
										Our team consists of market veterans who take their profession seriously and passionately.
									</p>
									<br>
								</div>
							</div>
							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_pink">
										<i class="icon-setup"></i>
									</div>
									<h3 class="item_title">Monitor 24/7</h3>
									<p class="mb-0">
										You can monitor your account 24/7 in all kind of devices. They are easy to understand.
									</p>
									<br>
								</div>
							</div>
							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_royalblue">
										<i class="icon-ring-2"></i>
									</div>
									<h3 class="item_title">Controls in Your Hands</h3>
									<p class="mb-0">
										Your account and funds are always in your control. You can stop PAMM and withdraw your capital anytime. All is automated.
									</p>
								</div>
							</div>
							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_pink">
										<i class="icon-partnership"></i>
									</div>
									<h3 class="item_title">Daily Updates</h3>
									<p class="mb-0">
										Your account details are send to you as reports every day at the end of the market sessions.
									</p>
									<br>
								</div>
							</div>
							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_pink">
										<i class="icon-shield"></i>
									</div>
									<h3 class="item_title">Secure Payments</h3>
									<p class="mb-0">
										Our payment methods are secure and safe with strict regulatory controls as well as easy to use.
									</p>
									<br>
								</div>
							</div>
							<div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="700">
								<div class="feature_boxed">
									<div class="item_icon icon_bg_pink">
										<i class="icon-setup"></i>
									</div>
									<h3 class="item_title">24/7 Support</h3>
									<p class="mb-0">
										Our team is all ready to assist you and answer your questions any time during the week. We are here to bring the world to you.
									</p>
								</div>
							</div>
						</div>
					</div>

				

				</section>
				<br>
				<!-- feature_section - end
				================================================== -->
<?php
include"footer.php";

?>