<?php
include"header.php";
include"admin/db.php";

?>
	<!-- breadcrumb_section - start
				================================================== -->
				<section id="breadcrumb_section" class="breadcrumb_section bg_gradient_blue deco_wrap d-flex align-items-center text-white clearfix">
					<div class="container">
						<?php
        	$sql=mysqli_query($conn,"SELECT * FROM news WHERE del=0 AND id='".$_GET['id']."'");

        	while ($row=mysqli_fetch_array($sql)) {
        		// print_r($row);exit;
        		?>
						<div class="breadcrumb_content text-center decrease_size" data-aos="fade-up" data-aos-delay="100">
							<p class="mb-30">
								<?php echo $row['name']; ?>
							</p>
							
						</div>
					</div>

					<div class="deco_image spahe_1" data-aos="fade-down" data-aos-delay="300">
						<img src="assets/images/shapes/shape_1.png" alt="spahe_not_found">
					</div>
					<div class="deco_image spahe_2" data-aos="fade-up" data-aos-delay="400">
						<img src="assets/images/shapes/shape_2.png" alt="spahe_not_found">
					</div>
				</section>
				<!-- breadcrumb_section - end
				================================================== -->
<!-- details_section - start
				================================================== -->
				<section class="details_section blog_details sec_ptb_120 clearfix">
					<div class="container">
						<div class="row justify-content-lg-between justify-content-md-center justify-content-sm-center">

							<div class="col-lg-8 col-md-10 col-sm-12">
								<div class="item_image mb-50" data-aos="fade-up" data-aos-delay="300">
									<img src="../admin/news/<?php echo $row['image'];?>" alt="image_not_found" style="width: 60%;" >
								</div>

							
								

								<div class="blog_quote mb-50" data-aos="fade-up" data-aos-delay="100">
									<span class="item_icon">
										<!-- <img src="assets/images/icons/quote_2.png" alt="icon_not_found"> -->
										<i class="fas fa-quote-left"></i>
									</span>
									<p class="mb-0">
										<a href="#!">
											<?php echo $row['des'];?>
										</a>
									</p>
								</div>

								<!-- <p class="mb-50">
									Why I say old chap that is spiffing pukka, bamboozled wind up bugger buggered zonked hanky panky a blinding shot the little rotter, bubble and squeak vagabond cheeky bugger at public school pardon your bloke the BBC. Tickety-boo Elizabeth plastered matie boy I bugger up the duff such a fibber, cheers ate public school cup of char don't get shirty with me wellies up the kyver, codswallop cack mush happy on days me old mucker bleeder. Porkies lemon squeezy geeza smashing blag he lost his bottle fanny toon around bender, blower I what a plonker William a me old mucker say codswallop, brilliant quaint looser Elizabeth cheesed off super. Only a quid bobby brilliant bugger Jeffrey owt to do with me lurgy blimey, cheers well me old mucker geeza bodge some dodgy chav. Say me old mucker bobby I a he lost his bottle a load of old tosh cup of char cheers bleeding bugger.
								</p> -->

								<div class="share_cmnt_btns d-flex justify-content-between align-items-center mb-30">
									<!-- <div class="col_wrap">
										<div class="share_links ul_li">
											<span class="list_title">SHARE:</span>
											<ul class="clearfix">
												<li><a href="#!"><i class="icon-facebook"></i></a></li>
												<li><a href="#!"><i class="icon-twitter"></i></a></li>
												<li><a href="#!"><i class="icon-vimeo"></i></a></li>
												<li><a href="#!"><i class="icon-linkedin"></i></a></li>
											</ul>
										</div>
									</div>
 -->
									<!-- <div class="col_wrap">
										<a href="#!" class="comment_btn"><i class="far fa-comment mr-1"></i> 03 Comments</a>
									</div> -->
								</div>

								<!-- <div class="item_tag ul_li mb-80">
									<span class="list_title">TAGS:</span>
									<ul class="clearfix">
										<li><a href="#!">makro</a></li>
										<li><a href="#!">Web Design</a></li>
										<li><a href="#!">Saas</a></li>
									</ul>
								</div> -->

								<!-- <h3 class="title_text mb-0">Related Posts</h3> -->
								<div class="row mb-80">
									<!-- <div class="col-lg-4 col-md-4 col-sm-6">
										<div class="blog_grid" data-aos="fade-up" data-aos-delay="100">
											<div class="post_date">
												<strong>14</strong>
												<span>Jan</span>
											</div>
											<a href="#!" class="item_image">
												<img src="assets/images/blogs/img_16.jpg" alt="image_not_found">
											</a>
											<div class="item_content">
												<h3 class="item_title">
													<a href="#!">Fast App development</a>
												</h3>
												<p class="mb-0">
													Why I say old chap that is spiffi pukka bamboozl...
												</p>
											</div>
										</div>
									</div> -->

									<!-- <div class="col-lg-4 col-md-4 col-sm-6">
										<div class="blog_grid" data-aos="fade-up" data-aos-delay="300">
											<div class="post_date">
												<strong>14</strong>
												<span>Jan</span>
											</div>
											<a href="#!" class="item_image">
												<img src="assets/images/blogs/img_17.jpg" alt="image_not_found">
											</a>
											<div class="item_content">
												<h3 class="item_title">
													<a href="#!">Fast App development</a>
												</h3>
												<p class="mb-0">
													Why I say old chap that is spiffi pukka bamboozl...
												</p>
											</div>
										</div>
									</div> -->

									<!-- <div class="col-lg-4 col-md-4 col-sm-6">
										<div class="blog_grid" data-aos="fade-up" data-aos-delay="500">
											<div class="post_date">
												<strong>14</strong>
												<span>Jan</span>
											</div>
											<div class="item_image">
												<img src="assets/images/blogs/img_18.jpg" alt="image_not_found">
												<a class="popup_video" href="https://www.youtube.com/watch?v=JOq6Q-YAg4s"><i class="icon-play"></i></a>
											</div>
											<div class="item_content">
												<h3 class="item_title">
													<a href="#!">Fast App development</a>
												</h3>
												<p class="mb-0">
													Why I say old chap that is spiffi pukka bamboozl...
												</p>
											</div>
										</div>
									</div> -->
								</div>

								
							</div>

							<div class="col-lg-4 col-md-10 col-sm-12">
								<aside id="sidebar_section" class="sidebar_section pl-30">

									<!-- <div class="widget sidebar_search form_item" data-aos="fade-up" data-aos-delay="300">
										<input type="search" name="search" placeholder="Search...">
										<button type="submit"><i class="ti-search"></i></button>
									</div> -->

									<div class="widget sidebar_blog ul_li_block" data-aos="fade-up" data-aos-delay="400">
										<h3 class="widget_title mb-30">Recent Posts</h3>
										<ul class="clearfix">
											<?php
											 $sqlcat=mysqli_query($conn,"SELECT * FROM news WHERE del=0 ORDER BY id DESC LIMIT 3"); 
                    while($rowcat=mysqli_fetch_array($sqlcat))
                    {
                        
                    ?>
											<li>

												<div class="blog_small">
													<a href="#!" class="item_image">
														<img src="../admin/news/<?php echo $rowcat['image'];?>" alt="image_not_found">
													</a>
													<div class="item_content">
														<h4 class="item_title">
															<a href="blogfullview.php?id=<?php echo $rowcat['id'];?>">
																<?php echo $rowcat['des'];?></a>
														</h4>
														<!-- <span class="post_date">July 06, 2020</span> -->
													</div>
												</div>
											</li>

											
											 <?php
                            
                        }
                        ?>
										</ul>
									</div>

									<!-- <div class="widget sidebar_category ul_li_block" data-aos="fade-up" data-aos-delay="100">
										<h3 class="widget_title mb-30">Categories</h3>
										<ul class="clearfix">
											<li><a href="#!">Fashion <span>(24)</span></a></li>
											<li><a href="#!">Food for thought <span>(09)</span></a></li>
											<li><a href="#!">Gaming <span>(07)</span></a></li>
											<li><a href="#!">Uncategorized <span>(02)</span></a></li>
											<li><a href="#!">makro <span>(04)</span></a></li>
											<li><a href="#!">Project Management <span>(07)</span></a></li>
											<li><a href="#!">Wireframing <span>(05)</span></a></li>
										</ul>
									</div> -->

									<!-- <div class="widget sidebar_tag" data-aos="fade-up" data-aos-delay="100">
										<h3 class="widget_title mb-30">Tags</h3>
										<div class="tag_list ul_li">
											<ul class="clearfix">
												<li><a href="#!">makro</a></li>
												<li><a href="#!">Web Design</a></li>
												<li><a href="#!">Saas</a></li>
												<li><a href="#!">Cooling System</a></li>
												<li><a href="#!">Corporate</a></li>
												<li><a href="#!">Software</a></li>
												<li><a href="#!">Landing</a></li>
												<li><a href="#!">Wheels</a></li>
											</ul>
										</div>
									</div> -->
									
								</aside>
							</div>

						</div>
							<?php
        	}
        	?>
					</div>
				</section>
				<!-- details_section - end
				================================================== -->


			</main>
			<!-- main body - end
			================================================== -->

<?php
include"footer.php";

?>