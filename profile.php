<?php
include"header1.php";
include"admin/db.php";
// session_start();
if(isset($_GET['id']))
{
    $_SESSION['userid']=$_GET['id'];
}
$ret=mysqli_query($conn,"SELECT * FROM member WHERE id='".$_SESSION['userid']."' ");
$num=mysqli_fetch_array($ret);
$ret1=mysqli_query($conn,"SELECT * FROM invest WHERE member_id='".$_SESSION['userid']."' LIMIT 1");
$num1=mysqli_fetch_array($ret1);
// print_r($num1);exit;
?>
 <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Profile</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="user-dashboard.php">Home</a></li>
                    <li class="breadcrumb-item active">My Profile</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
                            <div class="col-lg-12">
                                <div class="card border border-primary">
                                    <div class="card-header bg-transparent border-primary">
                                        <h5 class="my-0 text-primary"><i class="uil uil-user mr-3"></i>Personal Details </h5>
                                    </div>
                                    <div class="card-body">
                                    
                                        <p>Name 		:           <b><?php echo $num['fname']; ?></b><b><?php echo $num['lname']; ?></b></p>
                                        <p>Aadhar 		: <b><?php echo $num['aadhar']; ?></b></p>
                                        <p>Gender 		: <b><?php echo $num['gender']; ?></b></p>
                                        <p>Pan No 		: <b><?php echo $num['pan']; ?></b></p>
                                        <p>D.O.B 		: <b><?php echo date('d-m-Y',strtotime($num['dob'])); ?></b></p>
                                        <p>Weeding Date : <b><?php echo date('d-m-Y',strtotime($num['wdate'])); ?></b></p>
                                        <p>Mobile 		: <b><?php echo $num['mobile']; ?></b></p>
                                        
                                        <p>Email 		: <b><?php echo $num['email']; ?></b></p>
                                        <p>Occupation 	: <b><?php echo $num['occupation']; ?></b></p>
                                        <p>Address 		: <b><?php echo $num['address']; ?></b></p>
                                        <p>Pincode		: <b><?php echo $num['pincode']; ?></b></p>
                                        <p>Country 		: <b><?php echo $num['country']; ?></b></p>
                                        <p>State 		: <b><?php echo $num['state']; ?></b></p>
                                        <p>City 		: <b><?php echo $num['city']; ?></b></p>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col-lg-12">
                                <div class="card border border-danger">
                                    <div class="card-header bg-transparent border-danger">
                                        <h5 class="my-0 text-danger"><i class="uil uil-exclamation-octagon mr-3"></i>Nomiee Details</h5>
                                    </div>
                                    <div class="card-body">
                                        <p>Name 		:<b><?php echo $num['n_name']; ?></b></p>
                                        <p>Relationship		:<b><?php echo $num['n_relationship']; ?></b></p>
                                       
                                        	
                                        <p>Mobile 		:<b><?php echo $num['n_mobile']; ?></b></p>
                                        		
                                        <p>Email 		:<b><?php echo $num['n_email']; ?></b></p>
                                    </div>
                                </div>
                            </div>
        
                            <div class="col-lg-12">
                                <div class="card border border-success">
                                    <div class="card-header bg-transparent border-success">
                                        <h5 class="my-0 text-success"><i class="uil uil-check-circle mr-3"></i>Bank Details</h5>
                                    </div>
                                    <div class="card-body">
                                       <p>Holder Name 		:<b><?php echo $num['beneficiary_name']; ?></b></p>
                                        <p>Account		:<b><?php echo $num['account']; ?></b></p>
                                        <p>Bank Name 		:<b><?php echo $num['bankname']; ?></b></p>
                                         <p>Branch 		:<b><?php echo $num['branch']; ?></b></p>
                                        	
                                        <p>IFSC Code 		:<b><?php echo $num['ifsc_code']; ?></b></p>
                                        		
                                        <p>Swift/IBAN 		:<b><?php echo $num['swift_IBAN']; ?></b></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="card border border-success">
                                    <div class="card-header bg-transparent border-success">
                                        <h5 class="my-0 text-success"><i class="uil uil-check-circle mr-3"></i>1st Investment Details</h5>
                                    </div>
                                    <div class="card-body">
                                        
                                       <p>Platform Name       :<b><?php echo $num1['platname']; ?></b></p>
                                        <p>Investment Amount in INR      :<b><?php echo $num1['amt']; ?></b></p>
                                        <p>Investment Amount in USD       :<b><?php echo $num1['amt1']; ?></b></p>
                                         <p>Platform Password        :<b><?php echo $num1['pfp']; ?></b></p>
                                            
                                        <p>Investment Date        :<b><?php echo date('d-m-Y',strtotime($num1['i_date'])); ?></b></p>
                                                
                                        <!-- <p>Swift/IBAN       :<b><?php echo $num['swift_IBAN']; ?></b></p> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
            </div>


<?php
include"footer1.php";
?>