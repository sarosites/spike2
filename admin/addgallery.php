<?php


include"header.php";

include("db.php");

// start


  
// $pillar=mysqli_query($conn,"SELECT * FROM pillar WHERE fld_delete=0");
// $num=mysqli_fetch_array($ret);

  function compressImage($source, $destination, $quality) { 
    // Get image info 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
     
    // Create a new image from file 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break;
        case 'image/jpg': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    // Save image 
    imagejpeg($image, $destination, $quality); 
     
    // Return compressed image 
    return $destination; 
} 
 
 
// File upload path 

$uploadPath1 = "gallery/"; 
 
// If file upload form is submitted 
$status = $statusMsg = ''; 
if(isset($_POST["projectsub"])){ 
  $last_id=0;
  $name=$_POST['name'];

  
   
   
    $status = 'error'; 
    $images=array();

    // start loop
 if(isset($_FILES['image'])){
    for($i=0;$i<count($_FILES['image']['tmp_name']);$i++)
 { 
    if(!empty($_FILES["image"]["name"][$i])) { 
        // File info 
        $fileName1 = basename($_FILES["image"]["name"][$i]); 
        $imageUploadPath1 = $uploadPath1 . $fileName1; 
        $fileType1 = pathinfo($imageUploadPath1, PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes1 = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType1, $allowTypes1)){ 
            // Image temp source 
            $imageTemp11 = $_FILES["image"]["tmp_name"][$i]; 
            $imageTemp12 = $_FILES["image"]["name"][$i];
             
            // Compress size and upload image 
            $compressedImage1 = compressImage($imageTemp11, $imageUploadPath1, 40); 
             
            if($compressedImage1){ 
                $status = 'success'; 
                $statusMsg = "Image compressed successfully."; 
                 // $sql=mysqli_query($conn,"INSERT INTO gallery (title,images) VALUES ('".$title."','".$imageTemp12."')");
                $images[]=$imageTemp12;

              
            }else{ 
                $statusMsg = "Image compress failed!"; 
            } 
        }else{ 
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
        } 
    }else{ 
        $statusMsg = 'Please select an image file to upload.'; 
    } 
  }
}
// print_r($images);exit;

    // end loop

$imgg=implode('|', $images);
$sql=mysqli_query($conn,"INSERT INTO gallery (title,images) VALUES ('".$name."','".$imgg."')");
      echo  "<script>alert('Gallery Added Successfully')</script>";
  echo "<script>window.location.href='gallerylist.php'</script>";

} 
 
// Display status message 
echo $statusMsg; 

//end




?>

<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Add Blog</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="bloglist.php">Blog</a></li>
                    <li class="breadcrumb-item active">Add Blog</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
                       
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                       
        
                                        <form  action="" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="name">Gallery Name</label>
                                                <input type="text" name="name" id="name"  class="form-control" required placeholder="Enter name"      />
                                            </div>

                                             <div class="form-group">
                                                <label for="image">Gallery</label>
                                                <input type="file" name="image[]" multiple="true" id="image"  class="form-control" required placeholder="Enter"      />
                                            </div>                                           
                                           
        
                                           
        
                                            
                                            
                                            <div class="form-group mb-0">
                                                <div>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="projectsub">
                                                        Insert
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary waves-effect">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
        
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
        
                           
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->
<?php
include"footer.php";

?>