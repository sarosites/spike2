    <?php 
include"header.php";
include"db.php";
// session_start();
$pp=mysqli_query($conn,"SELECT * FROM feedback WHERE del=0 ");

if(isset($_GET['delid']))
{

$id=$_GET['delid'];

$sql=mysqli_query($conn,"UPDATE feedback SET del=1 WHERE id='$id'");
echo "<script>window.location.href='feedbacklist.php'</script>";
}
?>

     <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Feedback Info</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="managewebsite.php">managewebsite</a></li>
                    <li class="breadcrumb-item active">Feedback</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->                        
                       
        
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
        
                                        <h4 class="card-title">Feedback Details</h4>
                                        
                                        <p class="card-title-desc">
                                            <a href="addfeedback.php" class="btn btn-success">ADD</a>
                                        </p>
        
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Name</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                                
                                                
                                                <!-- <th>Action</th> -->
                                               
                                            </tr>
                                            </thead>
        
        
                                            <tbody>
                                            <?php
                                            if(!empty($pp))
                                            {
                                            $cnt=1;
                                             while ($num=mysqli_fetch_array($pp)) {
                                                   
                                                    // print_r($rr);exit;
                                            
                                            
                                           ?>

                                            <tr>
                                                <td><?php echo $cnt; ?></td> 
                                                 <td><?php echo $num['name'];?></td>
                                                  <td><?php echo $num['role'];?></td>
                                              
                                                
                                               
                                                <td><a href="viewfeedback.php?id=<?php echo $num['id'];?>"><i class="fa fa-eye fa-lg "></i> </a>

                                               <a href="editfeedback.php?uid=<?php echo $num['id'];?>"><i class="fa fa-edit fa-lg"></i> </a>
                                                <a href="feedbacklist.php?delid=<?php echo $num['id'];?>"> 
                                     <button class="btn btn-danger btn-sm" onClick="return confirm('Do you really want to delete');"><i class="fa fa-trash "></i></button></a>
                                  </td>
                                            </tr>
                                            <?php
                                            $cnt=$cnt+1;
                                                }
                                            }
                                            
                                            ?>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


<?php
include"footer.php";
?>