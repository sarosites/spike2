<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{

include"header.php";

include("db.php");

// start
  
// $pillar=mysqli_query($conn,"SELECT * FROM pillar WHERE fld_delete=0");
// $num=mysqli_fetch_array($ret);

  function compressImage($source, $destination, $quality) { 
    // Get image info 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
     
    // Create a new image from file 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break;
        case 'image/jpg': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    // Save image 
    imagejpeg($image, $destination, $quality); 
     
    // Return compressed image 
    return $destination; 
} 
 
 
// File upload path 
$uploadPath = "feedback/"; 
 
// If file upload form is submitted 
$status = $statusMsg = ''; 
if(isset($_POST["projectsub"])){ 
  // print_r($_POST);exit;
  $name=$_POST['name'];
  $role=$_POST['role'];
  $des=$_POST['des'];
   
    $status = 'error'; 
    if(!empty($_FILES["image"]["name"])) { 
        // File info 
        $fileName = basename($_FILES["image"]["name"]); 
        $imageUploadPath = $uploadPath . $fileName; 
        $fileType = pathinfo($imageUploadPath, PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType, $allowTypes)){ 
            // Image temp source 
            $imageTemp = $_FILES["image"]["tmp_name"]; 
            $imageTemp1 = $_FILES["image"]["name"];
             
            // Compress size and upload image 
            $compressedImage = compressImage($imageTemp, $imageUploadPath, 40); 
             
            if($compressedImage){ 
                $status = 'success'; 
                $statusMsg = "Image compressed successfully."; 
                // $img=$uploadPath.$imageTemp1;
                // $img="http://www.niftymaster.com/nifty/admin/".$uploadPath.$imageTemp1;

                 $sql=mysqli_query($conn,"INSERT INTO feedback (name,image,des,role) VALUES ('".$name."','".$imageTemp1."','".$des."','".$role."')");

              
            }else{ 
                $statusMsg = "Image compress failed!"; 
            } 
        }else{ 
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
        } 
    }else{ 
        $statusMsg = 'Please select an image file to upload.'; 
    } 
      echo  "<script>alert('feedback Added Successfully')</script>";
  echo "<script>window.location.href='feedbacklist.php'</script>";

} 
 
// Display status message 
echo $statusMsg; 

//end
  

?>

  <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Add Inversemnt</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="inversment.php">Inversment</a></li>
                    <li class="breadcrumb-item active">Add Inversemnt</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
                       
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                       
        
                                        <form  action="" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" name="name" id="name"  class="form-control" required placeholder="Enter name"      />
                                            </div>
                                            <div class="form-group">
                                                <label for="role">Role</label>
                                                <input type="text" name="role" id="role"  class="form-control" required placeholder="Enter Role"      />
                                            </div>
                                            <div class="form-group">
                                                <label for="image">Image</label>
                                                <input type="file" id="image" class="form-control" name="image"  required="">

                                            </div>
                                            <div class="form-group">
                                                <label for="des">Descrption</label>
                                                <input type="text" name="des" id="des" class="form-control" required placeholder="Enter Descrption"/>
                                            </div>

                                           
        
                                           
        
                                            
                                            
                                            <div class="form-group mb-0">
                                                <div>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="projectsub">
                                                        Insert
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary waves-effect">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
        
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
        
                           
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->
<?php
include"footer.php";
}
?>