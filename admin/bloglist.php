    <?php
include"header.php";
include"db.php";
// session_start();
$pp=mysqli_query($conn,"SELECT * FROM blog WHERE del=0 ");

if(isset($_GET['delid']))
{

$id=$_GET['delid'];

$sql=mysqli_query($conn,"UPDATE blog SET del=1 WHERE id='$id'");
echo "<script>window.location.href='bloglist.php'</script>";
}
?>

     <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Blog Info</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="managewebsite.php">Manage Website</a></li>
                    <li class="breadcrumb-item active">Bloglist</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->                        
                       
        
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
        
                                        <h4 class="card-title">Blog Details</h4>
                                        <a href="addblog.php" class="btn btn-primary">ADD</a>
                                        <p class="card-title-desc">
                                        </p>
        
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th> Name</th>
                                                 <th>Image</th>
                                                <th>Action</th>
                                                
                                                
                                                <!-- <th>Action</th> -->
                                               
                                            </tr>
                                            </thead>
        
        
                                            <tbody>
                                            <?php
                                            if(!empty($pp))
                                            {
                                            $cnt=1;
                                            while ($num=mysqli_fetch_array($pp)) {
                                                   
                                                    // print_r($rr);exit;
                                            ?>
                                            
                                           

                                            <tr>
                                                <td><?php echo $cnt; ?></td> 
                                                <td><?php echo $num['name'];?></td>
                                                <td><?php echo $num['image'];?></td>
                                                <td><a href="viewblog.php?id=<?php echo $num['id'];?>"><i class="fa fa-eye fa-lg "></i> </a>

                                               <a href="editblog.php?uid=<?php echo $num['id'];?>"><i class="fa fa-edit fa-lg"></i> </a>
                                                <a href="bloglist.php?delid=<?php echo $num['id'];?>"> 
                                     <button class="btn btn-danger btn-sm" onClick="return confirm('Do you really want to delete');"><i class="fa fa-trash "></i></button></a>
                                  </td>
                                            </tr>
                                            <?php
                                            $cnt=$cnt+1;
                                                }
                                            }
                                            ?>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


<?php
include"footer.php";
?>