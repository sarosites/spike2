    <?php
include"header.php";
include"db.php";
// session_start();
$ret=mysqli_query($conn,"SELECT * FROM commission WHERE c_delete=0 AND p_status='Approved'");


if(isset($_GET['delid']))
{

$id=$_GET['delid'];

$sql=mysqli_query($conn,"UPDATE commission SET c_delete=1 WHERE id='$id'");
echo "<script>window.location.href='pwithdraw.php'</script>";
}
?>

     <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Withdraw Info</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="admin_dashboard.php">dashboard</a></li>
                    <li class="breadcrumb-item active">Withdraw</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->                        
                       
        
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
        
                                        <h4 class="card-title">Withdraw Details</h4>
                                        
                                        <p class="card-title-desc">
                                        </p>
        
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>SNO</th>
                                                <th>Name</th>
                                                <th>Commission Amount</th>
                                                <th>Platform Name</th>
                                                <th>Commission Date</th>
                                                <th>Profit Percentage</th>
                                                <th>Profit Amount</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
        
        
                                            <tbody>
                                            <?php
                                            if(!empty($ret))
                                            {
                                            $cnt=1;
                                                while ($num=mysqli_fetch_array($ret)) {
                                                   $sql11=mysqli_query($conn,"SELECT fullname FROM member WHERE fld_delete=0 AND id='".$num['id']."'");
                                                    $rr=mysqli_fetch_array($sql11);
                                            ?>

                                            <tr>
                                                <td><?php echo $cnt; ?></td>
                                                <td><?php echo $rr['fullname']; ?></td>
                                                <td><?php echo $num['amt']; ?></td>
                                                <td><?php echo $num['platname']; ?></td>
                                                <td><?php echo $num['c_date']; ?></td>
                                                <td><?php echo $num['p_percentage']; ?></td>
                                                <td><?php echo $num['p_amt']; ?></td>
                                                <td><?php echo $num['p_status']; ?></td>

                                                <td><a href="withdraw.php?delid=<?php echo $num['id'];?>"> 
                                     <button class="btn btn-danger btn-sm" onClick="return confirm('Do you really want to delete');"><i class="fa fa-trash "></i></button></a>
                                  </td>
                                            </tr>
                                            <?php
                                            $cnt=$cnt+1;
                                                }
                                            }
                                            ?>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


<?php
include"footer.php";
?>