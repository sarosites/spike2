<?php
session_start();
?>


<!doctype html>

<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="utf-8" />
<title>Admin | Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
<meta content="Themesbrand" name="author" />
<!-- App favicon -->
<link rel="shortcut icon" href="assets/images/11-121x121.png">
        <!-- Light layout Bootstrap Css -->
                <link rel="stylesheet" href="assets/libs/twitter-bootstrap-wizard/prettify.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                 <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- plugin css -->
        <link href="assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/libs/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

        <!-- Responsive datatable examples -->
        <link href="assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />   

<link rel="stylesheet" href="assets/libs/owl.carousel/assets/owl.carousel.min.css">

        <link rel="stylesheet" href="assets/libs/owl.carousel/assets/owl.theme.default.min.css">
<link href="assets/css/bootstrap-dark.min.css" id="bootstrap-dark-style" disabled="disabled" rel="stylesheet" type="text/css" />
<link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="assets/css/app-dark.min.css" id="app-dark-style" disabled="disabled" rel="stylesheet" type="text/css" />
<link href="assets/css/app-rtl.min.css" id="app-rtl-style" disabled="disabled" rel="stylesheet" type="text/css" />
<link href="assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
<script src="assets/libs/select2/js/select2.min.js"></script>
<script src="assets/libs/jquery/jquery.min.js"></script>

    </head>

    <body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <i class="uil-shutter-alt spin-icon"></i>
                </div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="layout-wrapper">

            <header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
           <div class="navbar-brand-box">
                <a href="" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="../assets/images/11-121x121.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="../assets/images/11-121x121.png" alt="" height="20">
                    </span>
                </a>

                <a href="user-dashboard.php" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="../assets/images/11-121x121.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="../assets/images/11-121x121.png" alt="" height="20">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
            <form class="app-search d-none d-lg-block">
                <div class="position-relative">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="uil-search"></span>
                </div>
            </form>
        </div>

        <div class="d-flex">

            <div class="dropdown d-inline-block d-lg-none ml-2">
                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="uil-search"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                    aria-labelledby="page-header-search-dropdown">
                    
                    <form class="p-3">
                        <div class="form-group m-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>

                                </div>
                                <span>Your Role :</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            

            

            <div class="dropdown d-none d-lg-inline-block ml-1">
                
            </div>

            <div class="dropdown d-inline-block">
                
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                    aria-labelledby="page-header-notifications-dropdown">
                    <div class="p-3">
                        
                    </div>
                    <div data-simplebar style="max-height: 230px;">
                        
                        
                        
                      
                    </div>
                    
                </div>
            </div>

            <div class="dropdown d-inline-block">
                <b>Your Role Is : ADMIN </b>
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="assets/images/users/avatar-4.jpg"
                        alt="Header Avatar">
                    <span class="d-none d-xl-inline-block ml-1 font-weight-medium font-size-15">Marcus</span>
                    <i class="uil-angle-down d-none d-xl-inline-block font-size-15"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    <!-- item-->
                    <a class="dropdown-item" href="index.php"><i class="uil uil-user-circle font-size-18 align-middle text-mute                                                                                                      d mr-1"></i> <span class="align-middle">View Profile</span></a>
                    <a class="dropdown-item" href="logout.php"><i class="uil uil-sign-out-alt font-size-18 align-middle mr-1 text-muted"></i> <span class="align-middle">Sign out</span></a>
                </div>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                    <i class="uil-cog"></i>
                </button>
            </div>
            
        </div>
    </div>
</header>
<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <!-- LOGO -->
    <div class="navbar-brand-box">
        <a href="index.html" class="logo logo-dark">
            <span class="logo-sm">
                <img src="../assets/images/11-121x121.png" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="../assets/images/11-121x121.png" alt="" height="20"><span><b>Spike Returns</b>
            </span></span>
        </a>

        <a href="admin_dashboard.php" class="logo logo-light">
            <span class="logo-sm">
                <img src="../assets/images/11-121x121.png" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="../assets/images/11-121x121.png" alt="" height="20">
            </span>
        </a>
    </div>

    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
        <i class="fa fa-fw fa-bars"></i>
    </button>

    <div data-simplebar class="sidebar-menu-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <!-- <li class="menu-title">Menu</li> -->

                <li>
                    <a href="admin_dashboard.php">
                        <i class="fa fa-clock"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

<li>
                    <a href="managewebsite.php">
                        <i class="fa fa-clock"></i>
                        <span>Manage Website</span>
                    </a>
                </li>
                <li>
                   
                  
                </li>

                <!-- <li class="menu-title">Apps</li> -->
<li>
                    <a href="newmember.php" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span>New Members</span>
                    </a>
                </li>

<li>
                    <a href="member.php" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span>Members</span>
                    </a>
                </li>   
                <li>
                    <a href="rejectedmember.php">
                        <i class="fa fa-user"></i>
                        <span>Rejected Member</span>
                    </a>
                </li>
                
                <li>
                    <a href="pinvest.php" class="waves-effect">
                        <i class="fa fa-rocket"></i>
                        <span>New Investment</span>
                    </a>
                </li>
                  <li>
                    <a href="ainvest.php" class="waves-effect">
                        <i class="fa fa-rocket"></i>
                        <span>Investment</span>
                    </a>
                </li>
                <li>
                    <a href="rejectedinvest.php" class="waves-effect">
                        <i class="fa fa-rocket"></i>
                        <span>Rejected Investment</span>
                    </a>
                </li>
              
                 <li>
                    <a href="awithdraw.php" class="waves-effect">
                        <i class="fa fa-money"></i>
                        <span>Withdraw Statement</span>
                    </a>
                </li>
                 <li>
                    <a href="profit1.php" class="waves-effect">
                        <i class="fa fa-users"></i>
                        <span>Profit</span>
                    </a>
                </li>
                 <li>
                    <a href="profile.php" class="waves-effect">
                        <i class="fa fa-users"></i>
                        <span>My Profile</span>
                    </a>
                </li>
                <li>
                    <a href="help.php" class="waves-effect">
                        <i class="fa fa-users"></i>
                        <span>Support / Help</span>
                    </a>
                </li>
                <li>
                    <a href="report.php" class="waves-effect">
                        <i class="fa fa-users"></i>
                        <span>Report</span>
                    </a>
                </li>
                <li>
                    <a href="promotion.php" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span>Promotion</span>
                    </a>
                </li>
                 <li>
                    <a href="logout.php" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span>logout</span>
                    </a>
                </li>

                

                

                

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->