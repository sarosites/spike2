<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{

include"header.php";

include("db.php");
if(isset($_GET['uid']))
  {
    $id=$_GET['uid'];
    $editpillar=mysqli_query($conn,"SELECT * FROM news WHERE id='".$id."'");
    $row=mysqli_fetch_array($editpillar);
  }
// start
  
// $pillar=mysqli_query($conn,"SELECT * FROM pillar WHERE fld_delete=0");
// $num=mysqli_fetch_array($ret);

  function compressImage($source, $destination, $quality) { 
    // Get image info 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
     
    // Create a new image from file 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break;
        case 'image/jpg': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    // Save image 
    imagejpeg($image, $destination, $quality); 
     
    // Return compressed image 
    return $destination; 
} 
 
 
// File upload path 
$uploadPath = "news/"; 
 
// If file upload form is submitted 
$status = $statusMsg = ''; 
if(isset($_POST["projectsub"])){ 
  // print_r($_POST);exit;
    $id=$_POST['id'];
  $name=$_POST['name'];
  $des=$_POST['des'];
   $sql=mysqli_query($conn,"UPDATE news SET name='$name',des='$des' WHERE id='$id'");
    $status = 'error'; 
    if(!empty($_FILES["image"]["name"])) { 
        // File info 
        $fileName = basename($_FILES["image"]["name"]); 
        $imageUploadPath = $uploadPath . $fileName; 
        $fileType = pathinfo($imageUploadPath, PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType, $allowTypes)){ 
            // Image temp source 
            $imageTemp = $_FILES["image"]["tmp_name"]; 
            $imageTemp1 = $_FILES["image"]["name"];
             
            // Compress size and upload image 
            $compressedImage = compressImage($imageTemp, $imageUploadPath, 40); 
             
            if($compressedImage){ 
                $status = 'success'; 
                $statusMsg = "Image compressed successfully."; 
                // $img=$uploadPath.$imageTemp1;
                // $img="http://www.niftymaster.com/nifty/admin/".$uploadPath.$imageTemp1;

                 // $sql=mysqli_query($conn,"INSERT INTO blog (name,image,des) VALUES ('".$name."','".$imageTemp1."','".$des."')");
                 $sql=mysqli_query($conn,"UPDATE news SET name='$name',des='$des',image='$imageTemp1' WHERE id='$id'");
              
            }else{ 
                $sql=mysqli_query($conn,"UPDATE news SET name='$name',des='$des' WHERE id='$id'");
                $statusMsg = "Image compress failed!"; 
            } 
        }else{ 
            $sql=mysqli_query($conn,"UPDATE news SET name='$name',des='$des' WHERE id='$id'");
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
        } 
    }else{ 
        $sql=mysqli_query($conn,"UPDATE news SET name='$name',des='$des' WHERE id='$id'");
        $statusMsg = 'Please select an image file to upload.'; 
    } 
      echo  "<script>alert('Blog Updated Successfully')</script>";
  echo "<script>window.location.href='newslist.php'</script>";

} 
 
// Display status message 
echo $statusMsg; 

//end
  

?>

  <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Edit News</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="newslist.php">News</a></li>
                    <li class="breadcrumb-item active">News Blog</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
                       
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                       
        
                                        <form  action="" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <input type="hidden" name="id" id="id"  value="<?php echo $row['id']; ?>"       />
                                                <label for="name">Name</label>
                                                <input type="text" name="name" id="name"  class="form-control" required value="<?php echo $row['name']; ?>" placeholder="Enter name"      />
                                            </div>
                                            <div class="form-group">
                                                <label for="image">Image</label>
                                                <input type="file" id="image" class="form-control" name="image"  >

                                            </div>
                                            <div class="form-group">
                                                <label for="des">Descrption</label>
                                                <textarea name="des" id="des" class="form-control"><?php echo $row['des']; ?></textarea>
                                                
                                            </div>
                                            <div class="form-group row">
<label class="col-sm-2 col-form-label">Image</label>
<img src="news/<?php echo isset($row['image'])?$row['image']:""; ?>" width="150">
</div>

                                           
        
                                           
        
                                            
                                            
                                            <div class="form-group mb-0">
                                                <div>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1" name="projectsub">
                                                        Update
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary waves-effect">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
        
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
        
                           
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->
<?php
include"footer.php";
}
?>