<?php


include"header.php";

include("db.php");

  if(isset($_GET['id']))
  {
    $id=$_GET['id'];
    $editpillar=mysqli_query($conn,"SELECT * FROM gallery WHERE id='".$id."'");
    $row=mysqli_fetch_array($editpillar);
  }


?>
 <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Gallery Info</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="managewebsite.php">Manage Website</a></li>
                    <li class="breadcrumb-item active">Gallery</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->                        
                       
        
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
        
                                        <h4 class="card-title">Gallery Details</h4>
                                        
                                        <p class="card-title-desc">
                                        </p>
                              

<div class="form-group row">
<label class="col-sm-2 col-form-label">Gallery Name</label>
<label class="col-sm-10 col-form-label"><?php echo isset($row['title'])?$row['title']:""; ?></label>
</div>


<div class="form-group row">
  <label class="col-sm-2 col-form-label">Image Gallery</label>
  <div class="col-sm-10">
    <div class="row">

  <?php

                 $img=explode('|', $row['images']);
                for($i=0;$i<count($img);$i++)
                {?>
                  <div class="col-sm-4">
<img src="gallery/<?php echo isset($img[$i])?$img[$i]:""; ?>" alt="img" width="150">
</div>
                  <?php
}
                  ?>

</div>
</div>

<div class="form-group mb-0">
                                                <div>
                                                    <a href="gallerylist.php" class="btn btn-primary waves-effect waves-light mr-1" name="projectsub">
                                                        Back
                                                    </a>
                                                    
                                                </div>
                                            </div>


        
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>







<?php
include"footer.php";

?>