<?php

include"header.php";

include("db.php");
if(isset($_GET['delid']))
{

$id=$_GET['delid'];

$sql=mysqli_query($conn,"UPDATE gallery SET g_delete=1 WHERE id='$id'");
// $sql1=mysqli_query($conn,"UPDATE images SET fld_i_delete=1 WHERE galleryid='$id'");
echo "<script>window.location.href='gallery.php'</script>";
}
  
$gallery=mysqli_query($conn,"SELECT * FROM gallery WHERE g_delete=0 ORDER BY id DESC");
// $num=mysqli_fetch_array($ret);

?>
<div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Gallery Details</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="managewebsite.php">Manage Website</a></li>
                    <li class="breadcrumb-item active">Gallery</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->                        
                       
        
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
        
                                        <h4 class="card-title">Gallery Details</h4>
                                        <a href="addgallery.php" class="btn btn-primary">ADD</a>
                                        <p class="card-title-desc">
                                        </p>
        
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
     
<th>Sno</th>
<th>Title</th>

<th>Action</th>
</tr>
</thead>
<tbody>

<?php
$cnt=1;
                while($row=mysqli_fetch_array($gallery))
                {
                  
                  ?>
                              <tr>
                              <td><?php echo $cnt;?></td>
                                  <td><?php echo $row['title'];?></td>
                                  

                                  <!-- <td>
                                      <a href="viewgallery.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-secondary btn-sm"><i class="ti-eye"></i></button></a>
                                     <a href="editgallery.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-primary btn-sm"><i class="ti-pencil"></i></button></a>
                                     <a href="gallery.php?delid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-danger btn-sm" onClick="return confirm('Do you really want to delete');"><i class="ti-trash "></i></button></a>
                                  </td> -->
                                  <td><a href="viewgallery.php?id=<?php echo $row['id'];?>"><i class="fa fa-eye fa-lg "></i> </a>

                                               <a href="editgallery.php?uid=<?php echo $row['id'];?>"><i class="fa fa-edit fa-lg"></i> </a>
                                                <a href="gallerylist.php?delid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-danger btn-sm" onClick="return confirm('Do you really want to delete');"><i class="fa fa-trash "></i></button></a>
                                  </td>
                              </tr>
                              <?php $cnt=$cnt+1; }?>

</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->


<?php
include"footer.php";
?>