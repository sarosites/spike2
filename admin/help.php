<?php
include"header.php";
?>

        <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Help</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                    <li class="breadcrumb-item active">Help</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
                       

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-5">Support</h4>
                                        <div class="">
                                            <ul class="verti-timeline list-unstyled">
                                                <li class="event-list">
                                                    <div class="event-date text-primar">Phone</div>
                                                    <h5>+91 86670 31182</h5>
                                                          
                                                </li>
                                                <li class="event-list">
                                                    <div class="event-date text-primar">Email</div>
                                                    <h5>ksglobalscbe@gmail.com</h5>
                                                           
                                                </li>
                                                <li class="event-list">
                                                    <div class="event-date text-primar">24/7</div>
                                                    <h5>Support</h5>
                                                          
                                                </li>
                                                <li class="event-list">
                                                    <div class="event-date text-primar">Name</div>
                                                    <h5>KS Globals</h5>
                                                          
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


<?php
include"footer.php";
?>