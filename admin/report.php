<?php
include"header.php";
?>

        <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">

                        <!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0">Report</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                    <li class="breadcrumb-item active">Report</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
                       
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                       
        
                                        <form class="custom-validation" method="post" action="reportres.php">
                                            <div class="form-group">
                                                <select class="form-control" name="table">
                                                    <option>--Choose any one--</option>
                                                    <option value="member">Member</option>
                                                    <option value="withdraw">Withdraw</option>
                                                </select>
                                                
                                            </div>
        
                                            
        
                                            <div class="form-group">
                                                <label>Start Date</label>
                                                <div>
                                                    <input type="date" class="form-control" required
                                                            name="startdate" placeholder="Enter a valid e-mail"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>End Date</label>
                                                <div>
                                                    <input type="date" class="form-control" required
                                                            name="enddate" placeholder="Enter a valid e-mail"/>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div class="form-group mb-0">
                                                <div>
                                                    <button type="submit" name="report" class="btn btn-primary waves-effect waves-light mr-1">
                                                        Report
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary waves-effect">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
        
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div>
        
                           
                        
                    </div> <!-- container-fluid -->
                </div>
                <!-- End Page-content -->

<?php
include"footer.php";
?>